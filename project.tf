module "project" {
  source = "./project"

  providers = {
    aws         = aws
    aws.route53 = aws.route53  
  }

   name        = var.project
  environment = var.environment
  tags        = local.tags
  release     = var.release
}
