output "k8s_pod_node_selector" {
  value = module.project.k8s_pod_node_selector
}

output "k8s_pod_tolerations" {
  value = module.project.k8s_pod_tolerations
}

output "k8s_namespace" {
  value = module.project.k8s_namespace
}

output "calculation_abp" {
  value = module.project.calculation_abp
}

output "pe_core" {
  value = module.project.service_pe_core
}

output "database_technical_database" {
  value = module.project.database_technical_database
}