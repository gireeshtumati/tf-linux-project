provider "aws" {
  region      = var.aws_region
  profile     = "default"
  version     = "~> 2.0"
  max_retries = 15
}

provider "aws" {
  alias      = "route53"
  region     = "eu-west-1"
  version    = "~> 2.0"
  profile    = "route53"
  max_retries = 15
}

provider "null" {
  version = "~> 2.1.0"
}

provider "archive" {
  version = "~> 1.2.0"
}

provider "template" {
  version = "~> 2.1.0"
}

provider "local" {
  version = "~> 1.4.0"
}

terraform {
  required_version = "~> 0.12.0"

  backend "s3" {}
}
