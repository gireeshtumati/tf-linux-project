SHELL := /usr/bin/env bash

DATA_ROOT ?= .hiltiasws
KITCHEN_BIN ?= ./bin/kitchen.sh

.PHONY: login
login:
	$(KITCHEN_BIN) login

.PHONY: plan
plan:
	$(KITCHEN_BIN) tfi
	$(KITCHEN_BIN) tfp

.PHONY: apply
apply:
	$(KITCHEN_BIN) tfa

.PHONY: deploy
deploy:
	set -e; \
	for SERVICE in ./artifacts/ansible/* ; do \
		if [ "$$(basename $${SERVICE})" == "*" ]; then \
			break; \
		fi; \
		echo "Running ansible for $$(basename $${SERVICE}) service:"; \
		for RESOURCE in "$${SERVICE}"/* ; do \
			if [ ! -f "$${RESOURCE}/playbook.yml" ]; then \
			  continue; \
			fi; \
			cd "$${RESOURCE}"; \
			chmod 600 *.pem || true; \
			if [ "$${ANSIBLE_TAGS}" == "" ] && [ -f "tags" ]; then \
			  ANSIBLE_TAGS="$$(cat "tags")"; \
			fi; \
			ansible-playbook -i inventory.ini $${ANSIBLE_VERBOSITY} -v playbook.yml -e "@variables.json" --tags="$${ANSIBLE_TAGS}" $${ANSIBLE_EXTRA_VARS}; \
			cd -; \
		done; \
	done;
