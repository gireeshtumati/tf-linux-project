#!/usr/bin/env bash
set -e

DOCKER_IMAGE_KITCHEN="hiltiasws/pipeline-image-kitchen:beta-latest"
WORKDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)/.."

if [ "${DATA_ROOT:-}" == "" ]; then
  DATA_ROOT="$(readlink -f "${DATA_ROOT:-${WORKDIR}/.hiltiasws}" 2>/dev/null || greadlink -f "${DATA_ROOT:-${WORKDIR}/.hiltiasws}")"
else
  mkdir -p "${DATA_ROOT}"
  DATA_ROOT="$(realpath "${DATA_ROOT}")"
fi

ENVIRONMENT="${ENVIRONMENT:-dev}"
RELEASE=${RELEASE:-latest}
PROJECT=pe
VERSION="0.1.0"

if [ "${KITCHEN_VERSION:-}" != "" ]; then
  export WORKSPACE_HOME="${WORKDIR}"
  if [ "${ENVS_DIR:-}" == "" ]; then
    export ENVS_DIR="${DATA_ROOT}/envs"
  else
    mkdir -p "${ENVS_DIR}"
    ENVS_DIR="$(realpath "${ENVS_DIR}")"
  fi
  if [ "${RELEASES_DIR:-}" == "" ]; then
    export RELEASES_DIR="${DATA_ROOT}/releases"
  else
    mkdir -p "${RELEASES_DIR}"
    RELEASES_DIR="$(realpath "${RELEASES_DIR}")"
  fi
  export TF_ROOT="${INFRADIR}"
  export ENVIRONMENT
  export RELEASE
  export PROJECT
  export VERSION

  if [ "${ROUTE53_ACCESS_KEY:-}" != "" ]; then
    export TF_VAR_route53_access_key="${ROUTE53_ACCESS_KEY}"
  fi
  if [ "${ROUTE53_ACCESS_USER:-}" != "" ]; then
    export TF_VAR_route53_access_user="${ROUTE53_ACCESS_USER}"
  fi
  export TF_VAR_module_version="0.1.0-${TAG_NAME:-master}"

  "${KITCHEN}" "${@}"
else
  ARGS=()
  if [ "${ENVS_DIR:-}" != "" ]; then
    ARGS=("-v" "${ENVS_DIR}:/hilti/data-envs" "-e" "ENVS_DIR=/hilti/data-envs")
  fi

  docker run --rm -ti "${ARGS[@]}" \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v ${DATA_ROOT}:/hilti/data \
    -v "${WORKDIR}":/hilti/workspace \
    -v "${DOCKERDIR}":"${DOCKER_BUILD_CONTEXT}" \
    -e ENVIRONMENT="${ENVIRONMENT}" \
    -e RELEASE="${RELEASE}" \
    -e REMOTE="${REMOTE}" \
    -e DEBUG="${DEBUG}" \
    -e QUIET="${QUIET}" \
    -e PROJECT="${PROJECT}" \
    -e VERSION="${VERSION}" \
    -e HILTI_USERNAME="${HILTI_USERNAME}" \
    -e HILTI_PASSWORD="${HILTI_PASSWORD}" \
    -e HILTI_TOTP="${HILTI_TOTP}" \
    -p 8080-8090 \
    ${DOCKER_IMAGE_KITCHEN} \
    "${@}"
fi
