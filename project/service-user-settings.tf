/*
locals {
  service_user_settings_name = "user-settings"
}

module "service-user-settings" {
  source = "[MODULE_URL_PREFIX]/service-fargate/0.1.0-master.zip"

  providers = {
    aws         = aws
    aws.route53 = aws.route53
  }

  instances       = local.fargate_count 
  limits_cpu      = local.fargate_cpu
  limits_memory   = local.fargate_memory
  healthcheck_url = "/hc"

  oag_template_create = "true"

  project     = var.name
  name        = local.service_user_settings_name
  environment = var.environment
  tags        = local.tags
}

module "service-user-settings-variables" {
  source = "[MODULE_URL_PREFIX]/variables/0.1.0-master.zip"

  project     = var.name
  name        = local.service_user_settings_name
  environment = var.environment
  tags        = local.tags
  target_path = module.service-user-settings.variables_target_path

  variables = {
    "ASPNETCORE_ENVIRONMENT"              = var.environment
    "ASPNETCORE_SUPPRESSSTATUSMESSAGES"   = "true"
    "Database:ConnectionString"           = "${module.service-user-settings-database-rds.connection_string}"
    "Database:CreatedFromBackup"          = "true"
  }
}

module "service-user-settings-ecr" {
  source = "[MODULE_URL_PREFIX]/ecr/0.1.0-master.zip"

  project     = var.name
  name        = local.service_user_settings_name
  environment = var.environment
  tags        = local.tags
}

module "service-user-settings-database-rds" {
  source = "[MODULE_URL_PREFIX]/rds/0.1.0-master.zip"

    project     = var.name
    name        = local.service_user_settings_name
    environment = var.environment
    tags        = local.tags

    rds_output_name = "Hilti_PROFIS3_UserSettingsService"
    rds_engine = "sqlserver-web"
    rds_engine_version = "14.00.3223.3.v1"
    rds_instance_class = "db.t2.medium"
    rds_port = "1433"
    rds_allocated_storage = "30"
    rds_license = "license-included"
    rds_password_use_special = false
    rds_create_mssql_backup_bucket = true
}

output "service-user-settings" {
  value = {
    name              = local.service_user_settings_name

    ecr_url           = module.service-user-settings-ecr.url
    endpoint          = module.service-user-settings.endpoint
    endpoint_full     = module.service-user-settings.endpoint_full
    host              = module.service-user-settings.host
    database_host     = module.service-user-settings-database-rds.host

    oag_endpoint      = module.service-user-settings.oag_endpoint
    oag_endpoint_full = module.service-user-settings.oag_endpoint_full

    database_connection_string = module.service-user-settings-database-rds.connection_string
    database_user = module.service-user-settings-database-rds.user
    database_password = module.service-user-settings-database-rds.password
    database_host = module.service-user-settings-database-rds.host
    database_name = module.service-user-settings-database-rds.name
  }
}
*/