/*
locals {
  service_nolasoftreportservice_pdfreactor_name = "nolasoftreportservice-2"
  pdfreactor_webapiurl = "https://pe-services-pdfreactor.dev.swu.hilti.cloud/service/rest"
  pdfreactor_webapikey = "junwutest"
}

module "service-nolasoftreportservice_pdfreactor" {
  source = "[MODULE_URL_PREFIX]/service-fargate/0.1.0-master.zip"

  providers = {
    aws         = aws
    aws.route53 = aws.route53
  }

  instances       = local.fargate_count 
  limits_cpu      = local.fargate_cpu
  limits_memory   = local.fargate_memory
  healthcheck_url = "/hc"

  oag_template_create = "true"

  project     = var.name
  name        = local.service_nolasoftreportservice_pdfreactor_name
  environment = var.environment
  tags        = local.tags
}

module "service-nolasoftreportservice_pdfreactor-variables" {
  source = "[MODULE_URL_PREFIX]/variables/0.1.0-master.zip"

  project     = var.name
  name        = local.service_nolasoftreportservice_pdfreactor_name
  environment = var.environment
  tags        = local.tags
  target_path = module.service-nolasoftreportservice_pdfreactor.variables_target_path

  variables = {
    "ASPNETCORE_ENVIRONMENT" = var.environment
	"PDFReactor:WebAPIUrl" = local.pdfreactor_webapiurl
	"PDFReactor:ApiKey" = local.pdfreactor_webapikey
    }
}

module "service-nolasoftreportservice_pdfrecator-ecr" {
  source = "[MODULE_URL_PREFIX]/ecr/0.1.0-master.zip"

  project     = var.name
  name        = local.service_nolasoftreportservice_pdfreactor_name
  environment = var.environment
  tags        = local.tags
}

output "service-nolasoftreportservice_pdfreactor" {
  value = {
    name              = local.service_nolasoftreportservice_pdfreactor_name

    ecr_url           = module.service-nolasoftreportservice_pdfrecator-ecr.url
    endpoint          = module.service-nolasoftreportservice_pdfreactor.endpoint
    endpoint_full     = module.service-nolasoftreportservice_pdfreactor.endpoint_full
    host              = module.service-nolasoftreportservice_pdfreactor.host

    oag_endpoint      = module.service-nolasoftreportservice_pdfreactor.oag_endpoint
    oag_endpoint_full = module.service-nolasoftreportservice_pdfreactor.oag_endpoint_full
  }
}
*/