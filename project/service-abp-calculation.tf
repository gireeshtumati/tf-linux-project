/*
locals {
  service_abp_calculation_service_name = "abp-calculation-service"
  service_abp_calculation_elasticache_name = "abp-redis"
  service_abp_calculation_sns_name = "abp-calculation-notification"
  service_abp_calculation_sqs_calc_name = "abp-calculation-sqs-calc"
}

module "service-abp-calculation-service" {
  source = "[MODULE_URL_PREFIX]/service-fargate/0.1.0-master.zip"

  providers = {
    aws         = aws
    aws.route53 = aws.route53
  }

  instances       = local.fargate_count 
  limits_cpu      = local.fargate_cpu
  limits_memory   = local.fargate_memory
  healthcheck_url = "/hc"

  oag_template_create = "true"

  project     = var.name
  name        = local.service_abp_calculation_service_name
  environment = var.environment
  tags        = local.tags
}

module "service-abp-calculation-service-variables" {
  source = "[MODULE_URL_PREFIX]/variables/0.1.0-master.zip"

  project     = var.name
  name        = local.service_abp_calculation_service_name
  environment = var.environment
  tags        = local.tags
  target_path = module.service-abp-calculation-service.variables_target_path

  variables = {
    "ASPNETCORE_ENVIRONMENT"            = var.environment
    "Redis:Url"                         = module.service-abp-calculation-elasticache.url
    "SQS:Url"                           = module.service-abp-calculation-calc-sqs.url
    "SNS:TopicArn"                      = module.service-abp-calculation-sns.arn
  }
}

module "service-abp-calculation-service-ecr" {
  source = "[MODULE_URL_PREFIX]/ecr/0.1.0-master.zip"

  project     = var.name
  name        = local.service_abp_calculation_service_name
  environment = var.environment
  tags        = local.tags
}

module "service-abp-calculation-elasticache" {
  source = "[MODULE_URL_PREFIX]/elasticache/0.1.0-master.zip"

  elasticache_type = "cache.r5.xlarge"

  project     = var.name
  name        = local.service_abp_calculation_elasticache_name
  environment = var.environment
  tags        = local.tags
}

module "service-abp-calculation-sns" {
  source = "[MODULE_URL_PREFIX]/sns/0.1.0-master.zip"

  project     = var.name
  name        = local.service_abp_calculation_sns_name
  environment = var.environment
  tags        = local.tags
}

module "service-abp-calculation-calc-sqs" {
    source = "[MODULE_URL_PREFIX]/sqs/0.1.0-master.zip"

    project = var.name
    name    = local.service_abp_calculation_sqs_calc_name
    environment = var.environment
    tags        = local.tags

    subscription_sns_count = 1
    subscription_sns_arns  = ["${module.service-abp-calculation-sns.arn}"]
    subscription_sns_filter_policy = <<POLICY
{
  "service": [
    "abp-calculation-calc"
  ]
}
POLICY
}

output "service-abp-calculation" {
  value = {
      abp_calculation_service_ecr_url           = module.service-abp-calculation-service-ecr.url
      abp_calculation_service_endpoint          = module.service-abp-calculation-service.endpoint
      abp_calculation_service_endpoint_full     = module.service-abp-calculation-service.endpoint_full
      abp_calculation_service_host              = module.service-abp-calculation-service.host
      abp_calculation_service_oag_endpoint      = module.service-abp-calculation-service.oag_endpoint
      abp_calculation_service_oag_endpoint_full = module.service-abp-calculation-service.oag_endpoint_full

      abp_calculation_elasticache_url           = module.service-abp-calculation-elasticache.url

      abp_calculation_sns_topic_arn             = module.service-abp-calculation-sns.arn

      abp_calculation_calc_sqs_url              = module.service-abp-calculation-calc-sqs.url
      abp_calculation_calc_sqs_arn              = module.service-abp-calculation-calc-sqs.arn
  }
}
*/