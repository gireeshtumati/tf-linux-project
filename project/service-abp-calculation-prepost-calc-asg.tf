# locals {
#   service_abp_calculation_prepost_calc_asg = "abp-calculation-prepost-calc-asg"
# }

# module "service-abp-calculation-prepost-calc-asg" {
#   source = "[MODULE_URL_PREFIX]/ec2-asg/0.1.0-dev.zip" #!

#   providers = {
#     aws         = aws
#     aws.route53 = aws.route53
#   }

#   #project     = var.name
#   name        = local.service_abp_calculation_prepost_calc_asg
#   #environment = var.environment
#   tags        = local.tags

#   os_type             = "windows"
#   instance_type       = "t3.xlarge"
#   ec2_sg_min          = 1
#   ec2_sg_max          = 1
#   infra_bucket        = var.infra_bucket
#   route53_name        = var.route53_name
#   route53_zone        = var.route53_zone
#   vpc_private_subnets = var.vpc_private_subnets
#   aws_region          = var.aws_region
#   artifacts_dir       = var.artifacts_dir
#   acm_certificate     = var.acm_certificate
#   apps                = []
#   vpc_id              = var.vpc_id
#   vpc_subnets         = var.vpc_subnets
#   vpc_public_subnets  = var.vpc_public_subnets
#   permission_boundary = var.permission_boundary

#   health_check_type   = "EC2"
# }

# resource "aws_iam_role_policy_attachment" "abp-calculation-notification-attach-asg" {
#   role       = "abp-calculation-prepost-calc-asg"  #TODO: get instace role name from module module.service-abp-calculation-prepost-calc-asg.instance_role_name
#   policy_arn = aws_iam_policy.abp-calculation-prepost-calc-policy.arn
# }

# output "service-abp-calculation-prepost-calc-asg" {
#   value = {
#       abp_calculation_prepost_asg_ansible_config = module.service-abp-calculation-prepost-calc-asg.ansible_config
#       abp_calculation_prepost_asg_ec2_asg        = module.service-abp-calculation-prepost-calc-asg.ec2_asg
#       abp_calculation_prepost_asg_alb_hostname   = module.service-abp-calculation-prepost-calc-asg.alb_hostname
#       abp_calculation_prepost_asg_endpoint       = module.service-abp-calculation-prepost-calc-asg.endpoint
#   }
# }