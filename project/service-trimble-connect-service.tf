/*
locals {
  service_trimble_connect_service_name = "trimble-connect-service"
}

module "service-trimble-connect-service" {
  source = "[MODULE_URL_PREFIX]/service-fargate/0.1.0-master.zip"

  providers = {
    aws         = aws
    aws.route53 = aws.route53
  }

  instances       = local.fargate_count 
  limits_cpu      = local.fargate_cpu
  limits_memory   = local.fargate_memory
  healthcheck_url = "/hc"

  oag_template_create = "true"

  project     = var.name
  name        = local.service_trimble_connect_service_name
  environment = var.environment
  tags        = local.tags
}

module "service-trimble-connect-service-variables" {
  source = "[MODULE_URL_PREFIX]/variables/0.1.0-master.zip"

  project     = var.name
  name        = local.service_trimble_connect_service_name
  environment = var.environment
  tags        = local.tags
  target_path = module.service-trimble-connect-service.variables_target_path

  variables = {
    "ASPNETCORE_ENVIRONMENT"             = var.environment
    "ASPNETCORE_SUPPRESSSTATUSMESSAGES"  = "true"
  }
}

module "service-trimble-connect-service-ecr" {
  source = "[MODULE_URL_PREFIX]/ecr/0.1.0-master.zip"

  project     = var.name
  name        = local.service_trimble_connect_service_name
  environment = var.environment
  tags        = local.tags
}

output "service-trimble-connect-service" {
  value = {
    name              = local.service_trimble_connect_service_name

    ecr_url           = module.service-trimble-connect-service-ecr.url
    endpoint          = module.service-trimble-connect-service.endpoint
    endpoint_full     = module.service-trimble-connect-service.endpoint_full
    host              = module.service-trimble-connect-service.host

    oag_endpoint      = module.service-trimble-connect-service.oag_endpoint
    oag_endpoint_full = module.service-trimble-connect-service.oag_endpoint_full
  }
}
*/