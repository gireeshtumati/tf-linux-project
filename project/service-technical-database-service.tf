/*
locals {
  service_technical_database_service_name = "technical-database-service"
}

module "service-technical-database-service" {
  source = "[MODULE_URL_PREFIX]/service-fargate/0.1.0-master.zip"

  providers = {
    aws         = aws
    aws.route53 = aws.route53
  }

  instances       = local.fargate_count 
  limits_cpu      = local.fargate_cpu
  limits_memory   = local.fargate_memory
  healthcheck_url = "/hc"

  oag_template_create = "true"

  project     = var.name
  name        = local.service_technical_database_service_name
  environment = var.environment
  tags        = local.tags
}

module "service-technical-database-service-variables" {
  source = "[MODULE_URL_PREFIX]/variables/0.1.0-master.zip"

  project     = var.name
  name        = local.service_technical_database_service_name
  environment = var.environment
  tags        = local.tags
  target_path = module.service-technical-database-service.variables_target_path

  variables = {
    "ASPNETCORE_ENVIRONMENT"      = var.environment
    "Database:ConnectionString"   = "${module.database-technical-database-rds.connection_string}"
  }
}

module "service-technical-database-service-ecr" {
  source = "[MODULE_URL_PREFIX]/ecr/0.1.0-master.zip"

  project     = var.name
  name        = local.service_technical_database_service_name
  environment = var.environment
  tags        = local.tags
}

output "service-technical-database-service" {
  value = {
    name              = local.service_technical_database_service_name

    ecr_url           = module.service-technical-database-service-ecr.url
    endpoint          = module.service-technical-database-service.endpoint
    endpoint_full     = module.service-technical-database-service.endpoint_full
    host              = module.service-technical-database-service.host

    oag_endpoint      = module.service-technical-database-service.oag_endpoint
    oag_endpoint_full = module.service-technical-database-service.oag_endpoint_full
  }
}
*/