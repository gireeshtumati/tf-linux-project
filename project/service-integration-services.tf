/*
locals {
  service_integration_services_name = "integration-services"
  integration_services_data_sqs_name = "integration-services-data-sqs"
  integration_services_redis_storage_name = "pe-redis-storage"
  integration_services_documents_bucket_name = "integration-services-files"
}

module "service-integration-services" {
  source = "[MODULE_URL_PREFIX]/service-fargate/0.1.0-master.zip"

  providers = {
    aws         = aws
    aws.route53 = aws.route53
  }

  instances       = local.fargate_count 
  limits_cpu      = local.fargate_cpu
  limits_memory   = local.fargate_memory
  healthcheck_url = "/hc"

  oag_template_create = "true"

  project     = var.name
  name        = local.service_integration_services_name
  environment = var.environment
  tags        = local.tags
}

module "service-integration-services-variables" {
  source = "[MODULE_URL_PREFIX]/variables/0.1.0-master.zip"

  project     = var.name
  name        = local.service_integration_services_name
  environment = var.environment
  tags        = local.tags
  target_path = module.service-integration-services.variables_target_path

  variables = {
    "ASPNETCORE_ENVIRONMENT"              = var.environment
    "ASPNETCORE_SUPPRESSSTATUSMESSAGES"   = "true"
    "Database:ConnectionString"           = "${module.service-integration-services-database-rds.connection_string}"
    "Redis:Url"                           = module.service-integration-services-storage-elasticache.url
    "SQS:Url"                             = module.service-integration-services-data-sqs.url
    "Database:CreatedFromBackup"          = "true"  
    "S3:BucketName"                       = module.service-integration-services-documents-s3.bucket
    "UserSettingsService:Url"             = module.service-user-settings.endpoint
    "UserSettingsService:IsEnabled"       = "true"
    "Storage:Expiry"                      = "00:05:00"
  }
}

module "service-integration-services-data-sqs" {
  source = "[MODULE_URL_PREFIX]/sqs/0.1.0-master.zip"

  project     = var.name
  name        = local.integration_services_data_sqs_name
  environment = var.environment
  tags        = local.tags
}

module "service-integration-services-ecr" {
  source = "[MODULE_URL_PREFIX]/ecr/0.1.0-master.zip"

  project     = var.name
  name        = local.service_integration_services_name
  environment = var.environment
  tags        = local.tags
}

module "service-integration-services-storage-elasticache" {
  source = "[MODULE_URL_PREFIX]/elasticache/0.1.0-master.zip"

  elasticache_type = "cache.t2.medium"

  project     = var.name
  name        = local.integration_services_redis_storage_name
  environment = var.environment
  tags        = local.tags
}

module "service-integration-services-documents-s3" {
  source = "[MODULE_URL_PREFIX]/s3/0.1.0-master.zip"

  providers = {
    aws = aws
  }

  project     = var.name
  name        = local.integration_services_documents_bucket_name
  environment = var.environment
  tags        = local.tags
}

module "service-integration-services-database-rds" {
  source = "[MODULE_URL_PREFIX]/rds/0.1.0-master.zip"

  providers = {
    aws = aws
  }

  project     = var.name
  name        = local.service_integration_services_name
  environment = var.environment
  tags        = local.tags
    
  rds_output_name = "Hilti_PROFIS3_IntegrationServices"
  rds_engine = "sqlserver-web"
  rds_engine_version = "14.00.3223.3.v1"
  rds_instance_class = "db.t2.medium"
  rds_port = "1433"
  rds_allocated_storage = "30"
  rds_license = "license-included"
  rds_password_use_special = false
  rds_create_mssql_backup_bucket = true
}

output "service-integration-services" {
  value = {
    name              = local.service_integration_services_name

    ecr_url             = module.service-integration-services-ecr.url
    endpoint            = module.service-integration-services.endpoint
    endpoint_full       = module.service-integration-services.endpoint_full
    host                = module.service-integration-services.host
    database_host       = module.service-integration-services-database-rds.host

    sqs_url             = module.service-integration-services-data-sqs.url
    sqs_arn             = module.service-integration-services-data-sqs.arn

    pe_storage_url      = module.service-integration-services-storage-elasticache.url

    documents_s3_bucket = module.service-integration-services-documents-s3.bucket

    oag_endpoint        = module.service-integration-services.oag_endpoint
    oag_endpoint_full   = module.service-integration-services.oag_endpoint_full

    database_connection_string = module.service-integration-services-database-rds.connection_string
    database_user              = module.service-integration-services-database-rds.user
    database_password          = module.service-integration-services-database-rds.password
    database_host              = module.service-integration-services-database-rds.host
    database_name              = module.service-integration-services-database-rds.name
  }
}
*/