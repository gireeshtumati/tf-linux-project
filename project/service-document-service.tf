/*module "service-document-service" {
  source = "[MODULE_URL_PREFIX]/service/[ENVIRONMENT]-latest.zip"

  instances       = "${local.fargate_count}"
  limits_cpu      = "${local.fargate_cpu}"
  limits_memory   = "${local.fargate_memory}"
  healthcheck_url = "/hc"

  eslogs_create = "true"
  eslogs_lambda = "${module.eclogs.lambda_cloudwatch_to_es}"

  project = "${var.name}"
  name    = "document-service"
  tags    = "${local.tags}"
}

module "service-document-service-sqs" {
    source = "[MODULE_URL_PREFIX]/sqs/[ENVIRONMENT]-latest.zip"

    project = "${var.name}"
    name    = "document-service-sqs"

    subscription_sns_count = 1
    subscription_sns_arns  = ["${module.service-pe-gateway-sync-service-sns.arn}"]
    subscription_sns_filter_policy = <<POLICY
{
  "service": [
    "document-service"
  ],
  "transport": [
    "request"
  ]
}
POLICY
}

module "service-document-service-variables" {
  source = "[MODULE_URL_PREFIX]/variables/[ENVIRONMENT]-latest.zip"

  project     = "${var.name}"
  name        = "document-service"
  target_path = "${module.service-document-service.variables_target_path}"

  variables = {
    "ASPNETCORE_ENVIRONMENT"               = "${var.environment}"
    "ASPNETCORE_SUPPRESSSTATUSMESSAGES"    = "true"
    "Redis:Url"                            = "${module.service-pe-gateway-sync-service-elasticache.url}"
    "SNS:TopicArn"                         = "${module.service-pe-gateway-sync-service-sns.arn}"
    "SQS:Url"                              = "${module.service-document-service-sqs.url}"
  }

  tags = "${local.tags}"
}

module "service-document-service-ecr" {
  source = "[MODULE_URL_PREFIX]/ecr/[ENVIRONMENT]-latest.zip"

  project = "${var.name}"
  name    = "document-service"
  tags    = "${local.tags}"
}

output "service-document-service" {
  value = {
    ecr_url  = "${module.service-document-service-ecr.url}"
    endpoint = "${module.service-document-service.endpoint}"
  }
}
*/