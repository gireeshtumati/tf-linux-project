locals {
    calculation_abp_elasticache_name            = "abp-elasticache"
    calculation_abp_sns_name                    = "abp-sns"
    calculation_abp_sqs_calculation_name        = "abp-sqs-calculation"
    calculation_abp_sqs_prepostcalculation_name = "abp-sqs-prepostcalculation"
}

module "calculation-abp-elasticache" {
    source = "https://s3-eu-west-1.amazonaws.com/hilti-infra-public/shared-artifacts/terraform-modules/elasticache/0.1.0-china-stage.zip"

    elasticache_type = "cache.r5.xlarge"

    project     = var.name
    name        = local.calculation_abp_elasticache_name
    environment = var.environment
    tags        = local.tags
}

module "calculation-abp-sns" {
    source = "https://s3-eu-west-1.amazonaws.com/hilti-infra-public/shared-artifacts/terraform-modules/sns/0.1.0-china-stage.zip"

    project     = var.name
    name        = local.calculation_abp_sns_name
    environment = var.environment
    tags        = local.tags
}

module "calculation-abp-sqs-calculation" {
    source = "https://s3-eu-west-1.amazonaws.com/hilti-infra-public/shared-artifacts/terraform-modules/sqs/0.1.0-china-stage.zip"

    project     = var.name
    name        = local.calculation_abp_sqs_calculation_name
    environment = var.environment
    tags        = local.tags

    subscription_sns_count          = 1
    subscription_sns_arns           = ["${module.calculation-abp-sns.arn}"]
    subscription_sns_filter_policy  = <<POLICY
{
  "service": [
    "abp-calculation-calc"
  ]
}
POLICY
}

module "calculation-abp-sqs-prepostcalculation" {
    source = "https://s3-eu-west-1.amazonaws.com/hilti-infra-public/shared-artifacts/terraform-modules/sqs/0.1.0-china-stage.zip"

    project     = var.name
    name        = local.calculation_abp_sqs_prepostcalculation_name
    environment = var.environment
    tags        = local.tags

    subscription_sns_count          = 1
    subscription_sns_arns           = ["${module.calculation-abp-sns.arn}"]
    subscription_sns_filter_policy  = <<POLICY
{
  "service": [
    "abp-calculation-prepost-calc"
  ]
}
POLICY
}

output "calculation_abp" {
    value = {
        elasticache_url             = module.calculation-abp-elasticache.url
        sns_topic_arn               = module.calculation-abp-sns.arn
        sqs_calculation_url         = module.calculation-abp-sqs-calculation.url
        sqs_calculation_arn         = module.calculation-abp-sqs-calculation.arn
        sqs_prepostcalculation_url  = module.calculation-abp-sqs-prepostcalculation.url
        sqs_prepostcalculation_arn  = module.calculation-abp-sqs-prepostcalculation.arn
  }
}