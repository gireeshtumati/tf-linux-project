/*
locals {
  service_abp_coordinator_service_name = "abp-coordinator-service"
}

module "service-abp-coordinator-service" {
  source = "[MODULE_URL_PREFIX]/service-fargate/0.1.0-master.zip"

  providers = {
    aws         = aws
    aws.route53 = aws.route53
  }

  instances       = local.fargate_count 
  limits_cpu      = local.fargate_cpu
  limits_memory   = local.fargate_memory
  healthcheck_url = "/hc"

  oag_template_create = "true"

  project     = var.name
  name        = local.service_abp_coordinator_service_name
  environment = var.environment
  tags        = local.tags
}

module "service-abp-coordinator-service-variables" {
  source = "[MODULE_URL_PREFIX]/variables/0.1.0-master.zip"

  project     = var.name
  name        = local.service_abp_coordinator_service_name
  environment = var.environment
  tags        = local.tags
  target_path = module.service-abp-coordinator-service.variables_target_path

  variables = {
    "ASPNETCORE_ENVIRONMENT"            = var.environment
    "Redis:Url"                         = module.service-abp-calculation-elasticache.url
    "SNS:TopicArn"                      = module.service-abp-calculation-sns.arn
  }
}

module "service-abp-coordinator-service-ecr" {
  source = "[MODULE_URL_PREFIX]/ecr/0.1.0-master.zip"

  project     = var.name
  name        = local.service_abp_coordinator_service_name
  environment = var.environment
  tags        = local.tags
}

output "service-abp-coordinator" {
  value = {
      abp_coordinator_service_ecr_url           = module.service-abp-coordinator-service-ecr.url
      abp_coordinator_service_endpoint          = module.service-abp-coordinator-service.endpoint
      abp_coordinator_service_endpoint_full     = module.service-abp-coordinator-service.endpoint_full
      abp_coordinator_service_host              = module.service-abp-coordinator-service.host
      abp_coordinator_service_oag_endpoint      = module.service-abp-coordinator-service.oag_endpoint
      abp_coordinator_service_oag_endpoint_full = module.service-abp-coordinator-service.oag_endpoint_full
  }
}
*/