/*
locals {
  service_article_number_service_name = "article-number-service"
}

module "service-article-number-service" {
  source = "[MODULE_URL_PREFIX]/service-fargate/0.1.0-master.zip"

  providers = {
    aws         = aws
    aws.route53 = aws.route53
  }

  instances       = local.fargate_count 
  limits_cpu      = local.fargate_cpu
  limits_memory   = local.fargate_memory
  healthcheck_url = "/hc"

  oag_template_create = "true"

  project     = var.name
  name        = local.service_article_number_service_name
  environment = var.environment
  tags        = local.tags
}

module "service-article-number-service-variables" {
  source = "[MODULE_URL_PREFIX]/variables/0.1.0-master.zip"

  project     = var.name
  name        = local.service_article_number_service_name
  environment = var.environment
  tags        = local.tags
  target_path = module.service-article-number-service.variables_target_path

  variables = {
    "ASPNETCORE_ENVIRONMENT"      = var.environment
    "Database:ConnectionString"   = "${module.database-technical-database-rds.connection_string}"
  }
}

module "service-article-number-service-ecr" {
  source = "[MODULE_URL_PREFIX]/ecr/0.1.0-master.zip"

  project     = var.name
  name        = local.service_article_number_service_name
  environment = var.environment
  tags        = local.tags
}

output "service-article-number-service" {
  value = {
    name              = local.service_article_number_service_name

    ecr_url           = module.service-article-number-service-ecr.url
    endpoint          = module.service-article-number-service.endpoint
    endpoint_full     = module.service-article-number-service.endpoint_full
    host              = module.service-article-number-service.host

    oag_endpoint      = module.service-article-number-service.oag_endpoint
    oag_endpoint_full = module.service-article-number-service.oag_endpoint_full
  }
}
*/