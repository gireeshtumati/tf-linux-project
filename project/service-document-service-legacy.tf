/*locals {
  service_document_service_legacy_name = "document-service-legacy"
  document_service_legacy_sqs_name = "document-service-legacy-sqs"
}

module "service-document-service-legacy" {
  source = "[MODULE_URL_PREFIX]/service-fargate/0.1.0-master.zip"

  providers = {
    aws         = aws
    aws.route53 = aws.route53
  }

  instances       = local.fargate_count 
  limits_cpu      = local.fargate_cpu
  limits_memory   = local.fargate_memory
  healthcheck_url = "/hc"

  oag_template_create = "true"

  project     = var.name
  name        = local.service_document_service_legacy_name
  environment = var.environment
  tags        = local.tags
}

module "service-document-service-legacy-variables" {
  source = "[MODULE_URL_PREFIX]/variables/0.1.0-master.zip"

  project     = var.name
  name        = local.service_document_service_legacy_name
  environment = var.environment
  tags        = local.tags
  target_path = module.service-document-service-legacy.variables_target_path

  variables = {
    "ASPNETCORE_ENVIRONMENT"              = var.environment
    "ASPNETCORE_SUPPRESSSTATUSMESSAGES"   = "true"
    "Database:ConnectionString"           = "${module.service-document-service-legacy-database-rds.connection_string}"
    "Redis:Url"                           = module.service-pe-gateway-sync-service-elasticache.url
    "SNS:TopicArn"                        = module.service-pe-gateway-sync-service-sns.arn
    "SQS:Url"                             = module.service-document-service-legacy-sqs.url
    "Database:CreatedFromBackup"          = "true"  
  }
}

module "service-document-service-legacy-sqs" {
  source = "[MODULE_URL_PREFIX]/sqs/0.1.0-master.zip"

  project     = var.name
  name        = local.document_service_legacy_sqs_name
  environment = var.environment
  tags        = local.tags

  subscription_sns_count = 1
  subscription_sns_arns  = ["${module.service-pe-gateway-sync-service-sns.arn}"]
  subscription_sns_filter_policy = <<POLICY
{
  "service": [
    "document-service-legacy"
  ],
  "transport": [
    "request"
  ]
}
POLICY
}

module "service-document-service-legacy-ecr" {
  source = "[MODULE_URL_PREFIX]/ecr/0.1.0-master.zip"

  project     = var.name
  name        = local.service_document_service_legacy_name
  environment = var.environment
  tags        = local.tags
}

module "service-document-service-legacy-database-rds" {
  source = "[MODULE_URL_PREFIX]/rds/0.1.0-master.zip"

  project     = var.name
  name        = local.service_document_service_legacy_name
  environment = var.environment
  tags        = local.tags
    
  rds_output_name = "Hilti_PROFIS3_DocumentService"
  rds_engine = "sqlserver-web"
  rds_engine_version = "14.00.3223.3.v1"
  rds_instance_class = "db.t2.medium"
  rds_port = "1433"
  rds_allocated_storage = "60"
  rds_license = "license-included"
  rds_password_use_special = false
  rds_create_mssql_backup_bucket = true
}

output "service-document-service-legacy" {
  value = {
    name              = local.service_document_service_legacy_name

    ecr_url           = module.service-document-service-legacy-ecr.url
    endpoint          = module.service-document-service-legacy.endpoint
    endpoint_full     = module.service-document-service-legacy.endpoint_full
    host              = module.service-document-service-legacy.host
    database_host     = module.service-document-service-legacy-database-rds.host

    sqs_url           = module.service-document-service-legacy-sqs.url
    sqs_arn           = module.service-document-service-legacy-sqs.arn

    oag_endpoint      = module.service-document-service-legacy.oag_endpoint
    oag_endpoint_full = module.service-document-service-legacy.oag_endpoint_full

    database_connection_string = module.service-document-service-legacy-database-rds.connection_string
    database_user = module.service-document-service-legacy-database-rds.user
    database_password = module.service-document-service-legacy-database-rds.password
    database_host = module.service-document-service-legacy-database-rds.host
    database_name = module.service-document-service-legacy-database-rds.name
  }
}
*/