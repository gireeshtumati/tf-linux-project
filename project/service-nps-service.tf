
/*
locals {
  service_nps_service_name = "nps-service"
}

module "service-nps-service" {
  source = "[MODULE_URL_PREFIX]/service-fargate/0.1.0-master.zip"

  providers = {
    aws         = aws
    aws.route53 = aws.route53
  }

  instances       = local.fargate_count 
  limits_cpu      = local.fargate_cpu
  limits_memory   = local.fargate_memory
  healthcheck_url = "/hc"

  oag_template_create = "true"

  project     = var.name
  name        = local.service_nps_service_name
  environment = var.environment
  tags        = local.tags
}

module "service-nps-service-variables" {
  source = "[MODULE_URL_PREFIX]/variables/0.1.0-master.zip"

  project     = var.name
  name        = local.service_nps_service_name
  environment = var.environment
  tags        = local.tags
  target_path = module.service-nps-service.variables_target_path

  variables = {
    "ASPNETCORE_ENVIRONMENT"      = var.environment
    "UserSettingsService:Url"     = module.service-user-settings.endpoint
  }
}

module "service-nps-service-ecr" {
  source = "[MODULE_URL_PREFIX]/ecr/0.1.0-master.zip"

  project     = var.name
  name        = local.service_nps_service_name
  environment = var.environment
  tags        = local.tags
}

output "service-nps-service" {
  value = {
    name              = local.service_nps_service_name

    ecr_url           = module.service-nps-service-ecr.url
    endpoint          = module.service-nps-service.endpoint
    endpoint_full     = module.service-nps-service.endpoint_full
    host              = module.service-nps-service.host

    oag_endpoint      = module.service-nps-service.oag_endpoint
    oag_endpoint_full = module.service-nps-service.oag_endpoint_full
  }
}
*/