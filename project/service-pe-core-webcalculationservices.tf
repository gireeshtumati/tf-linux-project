/*
locals {
  service_pe_core_web_calculation_services_name = "pe-core-webcalculationservices"
}

module "service-pe-core-webcalculationservices" {
  source = "[MODULE_URL_PREFIX]/service-fargate/0.1.0-master.zip"

  providers = {
    aws         = aws
    aws.route53 = aws.route53
  }

  instances       = local.fargate_count
  limits_cpu      = local.fargate_cpu
  limits_memory   = local.fargate_memory
  healthcheck_url = "/hc"

  oag_template_create = "true"

  project     = var.name
  name        = local.service_pe_core_web_calculation_services_name
  environment = var.environment
  tags        = local.tags
}

module "service-pe-core-webcalculationservices-variables" {
  source = "[MODULE_URL_PREFIX]/variables/0.1.0-master.zip"

  project     = var.name
  name        = local.service_pe_core_web_calculation_services_name
  environment = var.environment
  tags        = local.tags
  target_path = module.service-pe-core-webcalculationservices.variables_target_path

  variables = {
    "ASPNETCORE_ENVIRONMENT"                       = var.environment
    "Redis:ConnectionString"                       = "${module.service-pe-core-storage-elasticache.url},resolveDns=true,syncTimeout=15000"
    "BaseplateDesign:RedisConnectionString"        = module.service-abp-calculation-elasticache.url
    "IntegrationServices:Url"                      = module.service-integration-services.endpoint
    "IntegrationServices:IsEnabled"                = "true"
    "TranslationsService:Url"                      = module.service-translations-service.endpoint
    "TransformationService:Url"                    = module.service-transformation-service.endpoint
    "BaseplateDesign:CoordinatorService:Url"       = module.service-abp-coordinator-service.endpoint
    "DataAccess:Profis3DatabaseConnectionString"   = module.service-pe-core-database-rds.connection_string
    "DataAccess:TechnicalDatabaseConnectionString" = module.database-technical-database-rds.connection_string
    "DataAccess:SignalRDatabaseConnectionString"   = module.service-pe-core-calc-sl-rds.connection_string
  }
}

module "service-pe-core-webcalculationservices-ecr" {
  source = "[MODULE_URL_PREFIX]/ecr/0.1.0-master.zip"

  project     = var.name
  name        = local.service_pe_core_web_calculation_services_name
  environment = var.environment
  tags        = local.tags
}

module "service-pe-core-calc-sl-rds" {
  source = "[MODULE_URL_PREFIX]/rds/0.1.0-master.zip"

  providers = {
    aws = aws
  }

  project     = var.name
  name        = "pe-core-calc"
  environment = var.environment
  tags        = local.tags

  rds_output_name = "Hilti_SignalR_Legacy"
  rds_engine = "sqlserver-web"
  rds_engine_version = "14.00.3223.3.v1"
  rds_instance_class = "db.t2.medium"
  rds_port = "1433"
  rds_allocated_storage = "30"
  rds_license = "license-included"
  rds_password_use_special = false
  rds_create_mssql_backup_bucket = true
}

output "service-pe-core-webcalculationservices" {
  value = {
    web_calculation_services_name              = local.service_pe_core_web_calculation_services_name

    web_calculation_services_ecr_url           = module.service-pe-core-webcalculationservices-ecr.url
    web_calculation_services_endpoint          = module.service-pe-core-webcalculationservices.endpoint
    web_calculation_services_endpoint_full     = module.service-pe-core-webcalculationservices.endpoint_full
    web_calculation_services_host              = module.service-pe-core-webcalculationservices.host

    web_calculation_sl_database_connection_string = module.service-pe-core-calc-sl-rds.connection_string
    web_calculation_sl_database_user              = module.service-pe-core-calc-sl-rds.user
    web_calculation_sl_database_password          = module.service-pe-core-calc-sl-rds.password
    web_calculation_sl_database_host              = module.service-pe-core-calc-sl-rds.host
    web_calculation_sl_database_name              = module.service-pe-core-calc-sl-rds.name

    web_calculation_services_oag_endpoint      = module.service-pe-core-webcalculationservices.oag_endpoint
    web_calculation_services_oag_endpoint_full = module.service-pe-core-webcalculationservices.oag_endpoint_full
  }
}
*/