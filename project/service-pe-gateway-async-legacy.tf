/*module "service-pe-gateway-async-legacy" {
  source = "[MODULE_URL_PREFIX]/service/[ENVIRONMENT]-latest.zip"

  instances       = "${local.fargate_count}"
  limits_cpu      = "${local.fargate_cpu}"
  limits_memory   = "${local.fargate_memory}"
  healthcheck_url = "/hc"

  create_oag_template = "true"

  eslogs_create = "true"
  eslogs_lambda = "${module.eclogs.lambda_cloudwatch_to_es}"

  project = "${var.name}"
  name    = "pe-gateway-async-legacy"
  tags    = "${local.tags}"
}

module "service-pe-gateway-async-legacy-variables" {
  source = "[MODULE_URL_PREFIX]/variables/[ENVIRONMENT]-latest.zip"

  project     = "${var.name}"
  name        = "pe-gateway-async-legacy"
  target_path = "${module.service-pe-gateway-async-legacy.variables_target_path}"

  variables = {
    "ASPNETCORE_ENVIRONMENT"               = "${var.environment}"
    "ASPNETCORE_SUPPRESSSTATUSMESSAGES"    = "true"
    "Redis:Url"                            = "${module.service-pe-gateway-sync-service-elasticache.url}"
    "SNS:TopicArn"                         = "${module.service-pe-gateway-sync-service-sns.arn}"
  }

  tags = "${local.tags}"
}

module "service-pe-gateway-async-legacy-ecr" {
  source = "[MODULE_URL_PREFIX]/ecr/[ENVIRONMENT]-latest.zip"

  project = "${var.name}"
  name    = "pe-gateway-async-legacy"
  tags    = "${local.tags}"
}

output "service-pe-gateway-async-legacy" {
  value = {
    ecr_url  = "${module.service-pe-gateway-async-legacy-ecr.url}"
    endpoint = "${module.service-pe-gateway-async-legacy.endpoint}"
    oag_endpoint      = "${module.service-pe-gateway-async-legacy.oag_endpoint}"
    oag_endpoint_full = "${module.service-pe-gateway-async-legacy.oag_endpoint_full}"
  }
}
*/