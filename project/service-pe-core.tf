locals {
  service_pe_core_name = "pe-core"
  service_pe_core_redis_storage_name = "pe-core-redis"
}

module "service-pe-core-storage-elasticache" {
  source = "https://s3-eu-west-1.amazonaws.com/hilti-infra-public/shared-artifacts/terraform-modules/elasticache/0.1.0-china-stage.zip"

  elasticache_type = "cache.t2.medium"

  project     = var.name
  name        = local.service_pe_core_redis_storage_name
  environment = var.environment
  tags        = local.tags
}

module "service-pe-core-database-rds" {
  source = "https://s3-eu-west-1.amazonaws.com/hilti-infra-public/shared-artifacts/terraform-modules/rds/0.1.0-china-stage.zip"

  providers = {
    aws = aws
  }

  project     = var.name
  name        = local.service_pe_core_name
  environment = var.environment
  tags        = local.tags
    
  rds_output_name = "Hilti_PROFIS3"
  rds_engine = "sqlserver-web"
  rds_engine_version = "14.00.3223.3.v1"
  rds_instance_class = "db.t2.medium"
  rds_port = "1433"
  rds_allocated_storage = "30"
  rds_license = "license-included"
  rds_password_use_special = false
  rds_create_mssql_backup_bucket = true
}

output "service_pe_core" {
  value = {

    database_connection_string = module.service-pe-core-database-rds.connection_string
    database_user              = module.service-pe-core-database-rds.user
    database_password          = module.service-pe-core-database-rds.password
    database_host              = module.service-pe-core-database-rds.host
    database_name              = module.service-pe-core-database-rds.name

    elasticache_url            = module.service-pe-core-storage-elasticache.url
  }
}