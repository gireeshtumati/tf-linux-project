/*
locals {
  service_pe_core_web_services_name = "pe-core-webservices"
}

module "service-pe-core-webservices" {
  source = "[MODULE_URL_PREFIX]/service-fargate/0.1.0-master.zip"

  providers = {
    aws         = aws
    aws.route53 = aws.route53
  }

  instances       = local.fargate_count 
  limits_cpu      = local.fargate_cpu
  limits_memory   = local.fargate_memory
  healthcheck_url = "/hc"

  oag_template_create = "true"

  project     = var.name
  name        = local.service_pe_core_web_services_name
  environment = var.environment
  tags        = local.tags
}

module "service-pe-core-webservices-variables" {
  source = "[MODULE_URL_PREFIX]/variables/0.1.0-master.zip"

  project     = var.name
  name        = local.service_pe_core_web_services_name
  environment = var.environment
  tags        = local.tags
  target_path = module.service-pe-core-webservices.variables_target_path

  variables = {
    "ASPNETCORE_ENVIRONMENT"                       = var.environment
    "DocumentService:Url"                          = module.service-document-service-legacy.endpoint
    "DocumentService:IsEnabled"                    = "true"
    "UserSettingsService:Url"                      = module.service-user-settings.endpoint
    "UserSettingsService:IsEnabled"                = "true"
    "Redis:ConnectionString"                       = "${module.service-pe-core-storage-elasticache.url},resolveDns=true,syncTimeout=15000"
    "IntegrationServices:Url"                      = module.service-integration-services.endpoint
    "IntegrationServices:IsEnabled"                = "true"
    "TranslationsService:Url"                      = module.service-translations-service.endpoint
    "BaseplateCalculationService:Url"              = module.service-pe-core-webcalculationservices.endpoint
    "DataAccess:Profis3DatabaseConnectionString"   = module.service-pe-core-database-rds.connection_string
    "DataAccess:TechnicalDatabaseConnectionString" = module.database-technical-database-rds.connection_string
  }
}

module "service-pe-core-webservices-ecr" {
  source = "[MODULE_URL_PREFIX]/ecr/0.1.0-master.zip"

  project     = var.name
  name        = local.service_pe_core_web_services_name
  environment = var.environment
  tags        = local.tags
}

output "service-pe-core-webservices" {
  value = {
    web_services_name              = local.service_pe_core_web_services_name

    web_services_ecr_url           = module.service-pe-core-webservices-ecr.url
    web_services_endpoint          = module.service-pe-core-webservices.endpoint
    web_services_endpoint_full     = module.service-pe-core-webservices.endpoint_full
    web_services_host              = module.service-pe-core-webservices.host

    web_services_oag_endpoint      = module.service-pe-core-webservices.oag_endpoint
    web_services_oag_endpoint_full = module.service-pe-core-webservices.oag_endpoint_full
  }
}
*/