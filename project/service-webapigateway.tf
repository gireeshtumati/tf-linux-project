/*
locals {
  service_webapigateway_name = "webapigateway"
  pe_apigateway_elasticache_name = "pe-redis"
  useredis = true
  webapigateway_jwttoken_signingpriavtekey = ""
  webapigateway_jwttoken_signingpriavtekeysecretname = "pe-signingpriavtekey"
  webapigateway_jwttoken_audiences = "urn:hc:api"
  webapigateway_jwttoken_issuer = "Hilti.PEAPI"
  webapigateway_jwttoken_usersa = false
  webapigateway_jwttoken_signingpriavtekeyxmlfile = "rsa-private-key.xml"
  webapigateway_claimprefix = "$PEAPI$"
  webapigateway_provider = "hilticloud"
  webapigateway_authority = "https://hilti-idm-sit.eu.auth0.com/"
  webapigateway_audiences = "urn:hc:api,https://hilti-idm-sit.eu.auth0.com/userinfo"
  webapigateway_authorizeresource = true
  webapigateway_authorizedpartykey = "azp"
  webapigateway_authorizedidentitykey = "https://cloud.hilti.com/logonId"
  webapigateway_gatewayaccesstokenheader = "GatewayAuthorization"
  webapigateway_dbconnstring = "mongodb+srv://junwuaccount:pa$$w0rd@hiltitest-tcj8a.mongodb.net/test?retryWrites=true&w=majority"
  webapigateway_dbname = "PEAPIIdentitiesDb"
  webapigateway_admin_authority = "https://junwutx.auth0.com/"
  webapigateway_admin_audience = "https://hilti-pe-gateway"
  webapigateway_admin_authorizedpartykey = "azp"
  webapigateway_admin_authorizedidentitykey = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier"
}


module "service-webapigateway" {
  source = "[MODULE_URL_PREFIX]/service-fargate/0.1.0-master.zip"

  providers = {
    aws         = aws
    aws.route53 = aws.route53
  }

  instances       = local.fargate_count 
  limits_cpu      = local.fargate_cpu
  limits_memory   = local.fargate_memory
  healthcheck_url = "/hc"

  oag_template_create = "true"

  project     = var.name
  name        = local.service_webapigateway_name
  environment = var.environment
  tags        = local.tags
}

module "service-webapigateway-variables" {
  source = "[MODULE_URL_PREFIX]/variables/0.1.0-master.zip"

  project     = var.name
  name        = local.service_webapigateway_name
  environment = var.environment
  tags        = local.tags
  target_path = module.service-webapigateway.variables_target_path

  variables = {
    "ASPNETCORE_ENVIRONMENT"              = var.environment
	"UseRedis"                            = local.useredis
    "Redis:Url"                           = module.service-pe-gateway-sync-service-elasticache.url
    "Authentication:JWTTokenUseRsa"       = local.webapigateway_jwttoken_usersa
    "Authentication:JWTTokenIssuer"       = local.webapigateway_jwttoken_issuer
    "Authentication:JWTTokenAudiences"    = local.webapigateway_jwttoken_audiences
    "Authentication:JWTTokenPrivateKey"   = local.webapigateway_jwttoken_signingpriavtekey
	"Authentication:JWTTokenPrivateKeySecretName"   = local.webapigateway_jwttoken_signingpriavtekeysecretname
    "Authentication:JWTTokenPrivateKeyXmlFile"   = local.webapigateway_jwttoken_signingpriavtekeyxmlfile
	"Authentication:ClaimPrefix"          = local.webapigateway_claimprefix
    "Authentication:Provider"             = local.webapigateway_provider
	"Authentication:Authority"            = local.webapigateway_authority
	"Authentication:Audiences"            = local.webapigateway_audiences
	"Authentication:AuthorizeResource"    = local.webapigateway_authorizeresource
    "Authentication:AuthorizedPartyKey"   = local.webapigateway_authorizedpartykey
	"Authentication:AuthorizedIdentityKey"   = local.webapigateway_authorizedidentitykey
"Authentication:GatewayAccessTokenHeader"   = local.webapigateway_gatewayaccesstokenheader
	"PEAPIServiceDatabaseSettings:ConnectionString" = local.webapigateway_dbconnstring
	"PEAPIServiceDatabaseSettings:DatabaseName" = local.webapigateway_dbname
    "Admin:Authority" = local.webapigateway_admin_authority
    "Admin:Audience" = local.webapigateway_admin_audience
    "Admin:AuthorizedPartyKey" = local.webapigateway_admin_authorizedpartykey
    "Admin:AuthorizedIdentityKey" = local.webapigateway_admin_authorizedidentitykey
  }
}

module "service-webapigateway-ecr" {
  source = "[MODULE_URL_PREFIX]/ecr/0.1.0-master.zip"

  project     = var.name
  name        = local.service_webapigateway_name
  environment = var.environment
  tags        = local.tags
}

output "service-webapigateway" {
  value = {
    name              = local.service_webapigateway_name

    ecr_url           = module.service-webapigateway-ecr.url
	elasticache       = module.service-pe-gateway-sync-service-elasticache.url
    endpoint          = module.service-webapigateway.endpoint
    endpoint_full     = module.service-webapigateway.endpoint_full
    host              = module.service-webapigateway.host

    oag_endpoint      = module.service-webapigateway.oag_endpoint
    oag_endpoint_full = module.service-webapigateway.oag_endpoint_full
  }
}
*/