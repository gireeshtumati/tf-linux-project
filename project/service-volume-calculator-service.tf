/*
locals {
  service_volume_calculator_service_name = "volume-calculator-service"
}

module "service-volume-calculator-service" {
  source = "[MODULE_URL_PREFIX]/service-fargate/0.1.0-master.zip"

  providers = {
    aws         = aws
    aws.route53 = aws.route53
  }

  instances       = local.fargate_count 
  limits_cpu      = local.fargate_cpu
  limits_memory   = local.fargate_memory
  healthcheck_url = "/hc"

  oag_template_create = "true"

  project     = var.name
  name        = local.service_volume_calculator_service_name
  environment = var.environment
  tags        = local.tags
}

module "service-volume-calculator-service-variables" {
  source = "[MODULE_URL_PREFIX]/variables/0.1.0-master.zip"

  project     = var.name
  name        = local.service_volume_calculator_service_name
  environment = var.environment
  tags        = local.tags
  target_path = module.service-volume-calculator-service.variables_target_path

  variables = {
    "ASPNETCORE_ENVIRONMENT"              = var.environment
  }
}

module "service-volume-calculator-service-ecr" {
  source = "[MODULE_URL_PREFIX]/ecr/0.1.0-master.zip"

  project     = var.name
  name        = local.service_volume_calculator_service_name
  environment = var.environment
  tags        = local.tags
}

output "service-volume-calculator-service" {
  value = {
    name              = local.service_volume_calculator_service_name

    ecr_url           = module.service-volume-calculator-service-ecr.url
    endpoint          = module.service-volume-calculator-service.endpoint
    endpoint_full     = module.service-volume-calculator-service.endpoint_full
    host              = module.service-volume-calculator-service.host

    oag_endpoint      = module.service-volume-calculator-service.oag_endpoint
    oag_endpoint_full = module.service-volume-calculator-service.oag_endpoint_full
  }
}
*/