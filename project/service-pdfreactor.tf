/*
locals {
  service_pdfreactor_name = "pdfreactor"
  pdfreactor_apikey="junwutest"
}

module "service-pdfreactor" {
  source = "[MODULE_URL_PREFIX]/service-fargate/0.1.0-master.zip"

  providers = {
    aws         = aws
    aws.route53 = aws.route53
  }

  instances       = local.fargate_count 
  limits_cpu      = local.fargate_cpu
  limits_memory   = local.fargate_memory
  healthcheck_url = "/service/rest/status?apiKey=${local.pdfreactor_apikey}"

  oag_template_create = "true"

  project     = var.name
  name        = local.service_pdfreactor_name
  environment = var.environment
  tags        = local.tags
}

module "service-pdfreactor-variables" {
  source = "[MODULE_URL_PREFIX]/variables/0.1.0-master.zip"

  project     = var.name
  name        = local.service_pdfreactor_name
  environment = var.environment
  tags        = local.tags
  target_path = module.service-pdfreactor.variables_target_path

  variables = {
    "ASPNETCORE_ENVIRONMENT"             = var.environment
    "ASPNETCORE_SUPPRESSSTATUSMESSAGES"  = "true"
  }
}

module "service-pdfreactor-ecr" {
  source = "[MODULE_URL_PREFIX]/ecr/0.1.0-master.zip"

  project     = var.name
  name        = local.service_pdfreactor_name
  environment = var.environment
  tags        = local.tags
}

output "service-pdfreactor" {
  value = {
    name              = local.service_pdfreactor_name

    ecr_url           = module.service-pdfreactor-ecr.url
    endpoint          = module.service-pdfreactor.endpoint
    endpoint_full     = module.service-pdfreactor.endpoint_full
    host              = module.service-pdfreactor.host

    oag_endpoint      = module.service-pdfreactor.oag_endpoint
    oag_endpoint_full = module.service-pdfreactor.oag_endpoint_full
  }
}
*/