
/*
locals {
  service_pe_ui_proxy_name = "pe-ui-proxy"
}

module "service-pe-ui-proxy" {
  source = "[MODULE_URL_PREFIX]/service-fargate/0.1.0-master.zip"

  providers = {
    aws         = aws
    aws.route53 = aws.route53
  }

  instances       = local.fargate_count
  limits_cpu      = local.fargate_cpu
  limits_memory   = local.fargate_memory
  healthcheck_url = "/"

  oag_template_create = "true"

  project     = var.name
  name        = local.service_pe_ui_proxy_name
  environment = var.environment
  tags        = local.tags
}

module "service-pe-ui-proxy-variables" {
  source = "[MODULE_URL_PREFIX]/variables/0.1.0-master.zip"

  project     = var.name
  name        = local.service_pe_ui_proxy_name
  environment = var.environment
  tags        = local.tags
  target_path = module.service-pe-ui-proxy.variables_target_path

  variables = {
    "ENVIRONMENT" = var.environment
    "S3_BUCKET"   = module.service-pe-ui-s3.bucket
    "S3_ENDPOINT" = "s3.eu-west-1.amazonaws.com"
  }
}

module "service-pe-ui-proxy-ecr" {
  source = "[MODULE_URL_PREFIX]/ecr/0.1.0-master.zip"

  project     = var.name
  name        = local.service_pe_ui_proxy_name
  environment = var.environment
  tags        = local.tags
}

output "service-pe-ui-proxy" {
  value = {
    name              = local.service_pe_ui_proxy_name

    ecr_url           = module.service-pe-ui-proxy-ecr.url
    endpoint          = module.service-pe-ui-proxy.endpoint
    endpoint_full     = module.service-pe-ui-proxy.endpoint_full
    host              = module.service-pe-ui-proxy.host

    oag_endpoint      = module.service-pe-ui-proxy.oag_endpoint
    oag_endpoint_full = module.service-pe-ui-proxy.oag_endpoint_full

  }
}
*/