/*
locals {
  service_product_information_name = "product-information"
}

module "service-product-information" {
  source = "[MODULE_URL_PREFIX]/service-fargate/0.1.0-master.zip"

  providers = {
    aws         = aws
    aws.route53 = aws.route53
  }

  instances       = local.fargate_count 
  limits_cpu      = local.fargate_cpu
  limits_memory   = local.fargate_memory
  healthcheck_url = "/hc"

  oag_template_create = "true"

  project     = var.name
  name        = local.service_product_information_name
  environment = var.environment
  tags        = local.tags
}

module "service-product-information-variables" {
  source = "[MODULE_URL_PREFIX]/variables/0.1.0-master.zip"

  project     = var.name
  name        = local.service_product_information_name
  environment = var.environment
  tags        = local.tags
  target_path = module.service-product-information.variables_target_path

  variables = {
    "ASPNETCORE_ENVIRONMENT"              = var.environment
    "ASPNETCORE_SUPPRESSSTATUSMESSAGES"   = "true"
    "Database:ConnectionString"           = "${module.service-product-information-database-rds.connection_string}"
    "Database:CreatedFromBackup"          = "true"
  }
}

module "service-product-information-ecr" {
  source = "[MODULE_URL_PREFIX]/ecr/0.1.0-master.zip"

  project     = var.name
  name        = local.service_product_information_name
  environment = var.environment
  tags        = local.tags
}

module "service-product-information-database-rds" {
  source = "[MODULE_URL_PREFIX]/rds/0.1.0-master.zip"

    project     = var.name
    name        = local.service_product_information_name
    environment = var.environment
    tags        = local.tags

    rds_output_name = "Hilti_PROFIS3_ProductInformation"
    rds_engine = "sqlserver-web"
    rds_engine_version = "14.00.3223.3.v1"
    rds_instance_class = "db.t2.medium"
    rds_port = "1433"
    rds_allocated_storage = "30"
    rds_license = "license-included"
    rds_password_use_special = false
    rds_create_mssql_backup_bucket = false
}

output "service-product-information" {
  value = {
    name              = local.service_product_information_name

    ecr_url           = module.service-product-information-ecr.url
    endpoint          = module.service-product-information.endpoint
    endpoint_full     = module.service-product-information.endpoint_full
    host              = module.service-product-information.host
    database_host     = module.service-product-information-database-rds.host

    oag_endpoint      = module.service-product-information.oag_endpoint
    oag_endpoint_full = module.service-product-information.oag_endpoint_full

    database_connection_string = module.service-product-information-database-rds.connection_string
    database_user = module.service-product-information-database-rds.user
    database_password = module.service-product-information-database-rds.password
    database_host = module.service-product-information-database-rds.host
    database_name = module.service-product-information-database-rds.name
  }
}
*/