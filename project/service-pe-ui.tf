
/*
locals {
  service_pe_ui_name = "pe-ui"
  service_pe_ui_bucket_name = "pe-ui-bucket"
  vpc_endpoints = {
    arch  = "vpce-0a63f0d8ae0011a0e"
    dev   = "vpce-0ca7d5507a36909bb"
    qa    = "vpce-0ea86db97ae61e079"
    stage = "vpce-085bf026606d80484"
    prod  = "vpce-0b2cc0db976fe06a6"
  }
  vpc_endpoint = lookup(local.vpc_endpoints, var.environment,"")
}

module "service-pe-ui" {
  source = "[MODULE_URL_PREFIX]/service-fargate/0.1.0-master.zip"

  providers = {
    aws         = aws
    aws.route53 = aws.route53
  }

  instances       = local.fargate_count
  limits_cpu      = local.fargate_cpu
  limits_memory   = local.fargate_memory
  healthcheck_url = "/"

  oag_template_create = "true"

  project     = var.name
  name        = local.service_pe_ui_name
  environment = var.environment
  tags        = local.tags
}

module "service-pe-ui-variables" {
  source = "[MODULE_URL_PREFIX]/variables/0.1.0-master.zip"

  project     = var.name
  name        = local.service_pe_ui_name
  environment = var.environment
  tags        = local.tags
  target_path = module.service-pe-ui.variables_target_path

  variables = {
    "ENVIRONMENT" = "pe-ui-${var.environment}"
  }
}

module "service-pe-ui-ecr" {
  source = "[MODULE_URL_PREFIX]/ecr/0.1.0-master.zip"

  project     = var.name
  name        = local.service_pe_ui_name
  environment = var.environment
  tags        = local.tags
}

module "service-pe-ui-s3" {
  source = "[MODULE_URL_PREFIX]/s3/0.1.0-master.zip"

  providers = {
    aws         = aws
  }

  project     = var.name
  name        = local.service_pe_ui_bucket_name
  environment = var.environment
  tags        = local.tags
}

resource "aws_s3_bucket_policy" "service-pe-ui-s3-policy" {
  bucket = module.service-pe-ui-s3.bucket

  policy = <<POLICY
{
  "Version": "2008-10-17",
  "Statement": [
      {
        "Effect": "Allow",
        "Principal": "*",
        "Action": "s3:GetObject",
        "Resource": "arn:aws:s3:::${module.service-pe-ui-s3.bucket}/*",
        "Condition": {
          "StringEquals": {
            "aws:sourceVpce": "${local.vpc_endpoint}"
          }
        }
      }
  ]
}
POLICY
}

output "service-pe-ui" {
  value = {
    name              = local.service_pe_ui_name

    ecr_url           = module.service-pe-ui-ecr.url
    endpoint          = module.service-pe-ui.endpoint
    endpoint_full     = module.service-pe-ui.endpoint_full
    host              = module.service-pe-ui.host

    oag_endpoint      = module.service-pe-ui.oag_endpoint
    oag_endpoint_full = module.service-pe-ui.oag_endpoint_full

    pe_ui_s3_bucket   = module.service-pe-ui-s3.bucket
  }
}
*/