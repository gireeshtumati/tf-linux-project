/*
locals {
  service_sns_receiver_name = "sns-receiver"
  sns_receiver_sqs_name = "sns-receiver-sqs"
}

module "service-sns-receiver" {
  source = "[MODULE_URL_PREFIX]/service-fargate/0.1.0-master.zip"

  providers = {
    aws         = aws
    aws.route53 = aws.route53
  }

  instances       = local.fargate_count
  limits_cpu      = local.fargate_cpu
  limits_memory   = local.fargate_memory
  healthcheck_url = "/hc"

  oag_template_create = "true"
  
  project     = var.name
  name        = local.service_sns_receiver_name
  environment = var.environment
  tags        = local.tags
}

module "service-sns-receiver-variables" {
  source = "[MODULE_URL_PREFIX]/variables/0.1.0-master.zip"

  project     = var.name
  name        = local.service_sns_receiver_name
  environment = var.environment
  tags        = local.tags
  target_path = module.service-sns-receiver.variables_target_path

  variables = {
    "ASPNETCORE_ENVIRONMENT"               = var.environment
    "ASPNETCORE_SUPPRESSSTATUSMESSAGES"    = "true"
    "Redis:Url"                            = module.service-pe-gateway-sync-service-elasticache.url
    "SQS:Url"                              = module.service-sns-receiver-sqs.url
  }
}

module "service-sns-receiver-sqs" {
  source = "[MODULE_URL_PREFIX]/sqs/0.1.0-master.zip"

  project     = var.name
  name        = local.sns_receiver_sqs_name
  environment = var.environment
  tags        = local.tags

  subscription_sns_count = 1
  subscription_sns_arns  = ["${module.service-pe-gateway-sync-service-sns.arn}"]
  subscription_sns_filter_policy = <<POLICY
{
  "transport": [
    "response"
  ]
}
POLICY
}

module "service-sns-receiver-ecr" {
  source = "[MODULE_URL_PREFIX]/ecr/0.1.0-master.zip"

  project     = var.name
  name        = local.service_sns_receiver_name
  environment = var.environment
  tags        = local.tags
}

output "service-sns-receiver" {
  value = {
    ecr_url              = module.service-sns-receiver-ecr.url
    endpoint             = module.service-sns-receiver.endpoint
    sqs_url              = module.service-sns-receiver-sqs.url
    sqs_arn              = module.service-sns-receiver-sqs.arn

    oag_endpoint         = module.service-sns-receiver.oag_endpoint
    oag_endpoint_full    = module.service-sns-receiver.oag_endpoint_full
  }
}
*/