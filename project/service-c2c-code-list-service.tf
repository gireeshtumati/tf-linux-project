/*
locals {
  service_c2c_code_list_service_name = "c2c-code-list-service"
}

module "service-c2c-code-list-service" {
  source = "[MODULE_URL_PREFIX]/service-fargate/0.1.0-master.zip"

  providers = {
    aws         = aws
    aws.route53 = aws.route53
  }

  instances       = local.fargate_count 
  limits_cpu      = local.fargate_cpu
  limits_memory   = local.fargate_memory
  healthcheck_url = "/hc"

  oag_template_create = "true"

  project     = var.name
  name        = local.service_c2c_code_list_service_name
  environment = var.environment
  tags        = local.tags
}

module "service-c2c-code-list-service-variables" {
  source = "[MODULE_URL_PREFIX]/variables/0.1.0-master.zip"

  project     = var.name
  name        = local.service_c2c_code_list_service_name
  environment = var.environment
  tags        = local.tags
  target_path = module.service-c2c-code-list-service.variables_target_path

  variables = {
    "ASPNETCORE_ENVIRONMENT"      = var.environment
  }
}

module "service-c2c-code-list-service-rds" {
  source = "[MODULE_URL_PREFIX]/rds/0.1.0-master.zip"

  project     = var.name
  name        = local.service_c2c_code_list_service_name
  environment = var.environment
  tags        = local.tags

  rds_engine   = "postgres"
  rds_password = "fphqxe8wc5sn0wdbtmgnxbfdgyud5cxx8udp9ww2"
  rds_db_name  = "Hilti_C2C_CodeListService"
}

module "service-c2c-code-list-service-ecr" {
  source = "[MODULE_URL_PREFIX]/ecr/0.1.0-master.zip"

  project     = var.name
  name        = local.service_c2c_code_list_service_name
  environment = var.environment
  tags        = local.tags
}

output "service-c2c-code-list-service" {
  value = {
    name              = local.service_c2c_code_list_service_name

    ecr_url           = module.service-c2c-code-list-service-ecr.url
    endpoint          = module.service-c2c-code-list-service.endpoint
    endpoint_full     = module.service-c2c-code-list-service.endpoint_full
    host              = module.service-c2c-code-list-service.host

    oag_endpoint      = module.service-c2c-code-list-service.oag_endpoint
    oag_endpoint_full = module.service-c2c-code-list-service.oag_endpoint_full
  }
}
*/