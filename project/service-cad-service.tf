/*
locals {
  service_cad_service_name = "cad-service"
}

module "service-cad-service" {
  source = "[MODULE_URL_PREFIX]/service-fargate/0.1.0-master.zip"

  providers = {
    aws         = aws
    aws.route53 = aws.route53
  }

  instances       = local.fargate_count 
  limits_cpu      = local.fargate_cpu
  limits_memory   = local.fargate_memory
  healthcheck_url = "/hc"

  oag_template_create = "true"

  project     = var.name
  name        = local.service_cad_service_name
  environment = var.environment
  tags        = local.tags
}

module "service-cad-service-variables" {
  source = "[MODULE_URL_PREFIX]/variables/0.1.0-master.zip"

  project     = var.name
  name        = local.service_cad_service_name
  environment = var.environment
  tags        = local.tags
  target_path = module.service-cad-service.variables_target_path

  variables = {
    "ASPNETCORE_ENVIRONMENT"      = var.environment
  }
}

module "service-cad-service-ecr" {
  source = "[MODULE_URL_PREFIX]/ecr/0.1.0-master.zip"

  project     = var.name
  name        = local.service_cad_service_name
  environment = var.environment
  tags        = local.tags
}

output "service-cad-service" {
  value = {
    name              = local.service_cad_service_name

    ecr_url           = module.service-cad-service-ecr.url
    endpoint          = module.service-cad-service.endpoint
    endpoint_full     = module.service-cad-service.endpoint_full
    host              = module.service-cad-service.host

    oag_endpoint      = module.service-cad-service.oag_endpoint
    oag_endpoint_full = module.service-cad-service.oag_endpoint_full
  }
}
*/