/*
locals {
  service_quantity_calculator_ui_name = "quantity-calculator-ui"
}

module "service-quantity-calculator-ui" {
  source = "[MODULE_URL_PREFIX]/service-fargate/0.1.0-master.zip"

  providers = {
    aws         = aws
    aws.route53 = aws.route53
  }

  instances       = local.fargate_count
  limits_cpu      = local.fargate_cpu
  limits_memory   = local.fargate_memory
  healthcheck_url = "/"

  oag_template_create = "true"

  project     = var.name
  name        = local.service_quantity_calculator_ui_name
  environment = var.environment
  tags        = local.tags
}

module "service-quantity-calculator-ui-variables" {
  source = "[MODULE_URL_PREFIX]/variables/0.1.0-master.zip"

  project     = var.name
  name        = local.service_quantity_calculator_ui_name
  environment = var.environment
  tags        = local.tags
  target_path = module.service-quantity-calculator-ui.variables_target_path

  variables = {
    "ENVIRONMENT" = "quantity-calculator-ui-${var.environment}"
  }
}

module "service-quantity-calculator-ui-ecr" {
  source = "[MODULE_URL_PREFIX]/ecr/0.1.0-master.zip"

  project     = var.name
  name        = local.service_quantity_calculator_ui_name
  environment = var.environment
  tags        = local.tags
}

output "service-quantity-calculator-ui" {
  value = {
    name              = local.service_quantity_calculator_ui_name

    ecr_url           = module.service-quantity-calculator-ui-ecr.url
    endpoint          = module.service-quantity-calculator-ui.endpoint
    endpoint_full     = module.service-quantity-calculator-ui.endpoint_full
    host              = module.service-quantity-calculator-ui.host

    oag_endpoint      = module.service-quantity-calculator-ui.oag_endpoint
    oag_endpoint_full = module.service-quantity-calculator-ui.oag_endpoint_full
  }
}
*/