/*locals {
  service_nolasoftreportservice_name = "nolasoftreportservice"
}

module "service-nolasoftreportservice" {
  source = "[MODULE_URL_PREFIX]/service-fargate/0.1.0-master.zip"

  providers = {
    aws         = aws
    aws.route53 = aws.route53
  }

  instances       = local.fargate_count 
  limits_cpu      = local.fargate_cpu
  limits_memory   = local.fargate_memory
  healthcheck_url = "/hc"

  oag_template_create = "true"

  project     = var.name
  name        = local.service_nolasoftreportservice_name
  environment = var.environment
  tags        = local.tags
}

module "service-nolasoftreportservice-variables" {
  source = "[MODULE_URL_PREFIX]/variables/0.1.0-master.zip"

  project     = var.name
  name        = local.service_nolasoftreportservice_name
  environment = var.environment
  tags        = local.tags
  target_path = module.service-nolasoftreportservice.variables_target_path

  variables = {
    "ASPNETCORE_ENVIRONMENT" = var.environment
    }
}

module "service-nolasoftreportservice-ecr" {
  source = "[MODULE_URL_PREFIX]/ecr/0.1.0-master.zip"

  project     = var.name
  name        = local.service_nolasoftreportservice_name
  environment = var.environment
  tags        = local.tags
}

output "service-nolasoftreportservice" {
  value = {
    name              = local.service_nolasoftreportservice_name

    ecr_url           = module.service-nolasoftreportservice-ecr.url
    endpoint          = module.service-nolasoftreportservice.endpoint
    endpoint_full     = module.service-nolasoftreportservice.endpoint_full
    host              = module.service-nolasoftreportservice.host

    oag_endpoint      = module.service-nolasoftreportservice.oag_endpoint
    oag_endpoint_full = module.service-nolasoftreportservice.oag_endpoint_full
  }
}
*/