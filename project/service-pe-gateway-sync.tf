/*
locals {
  pe_gateway_sync_service_name     = "pe-gateway-sync-service"
  pe_gateway_sync_elasticache_name = "pe-redis"
  pe_gateway_sync_sns_name         = "pe-messaging" 
}

module "service-pe-gateway-sync-service" {
  source = "[MODULE_URL_PREFIX]/service-fargate/0.1.0-master.zip"

  providers = {
    aws         = aws
    aws.route53 = aws.route53
  }

  instances       = local.fargate_count
  limits_cpu      = local.fargate_cpu
  limits_memory   = local.fargate_memory
  healthcheck_url = "/hc"

  oag_template_create = "true"

  project     = var.name
  name        = local.pe_gateway_sync_service_name
  environment = var.environment
  tags        = local.tags
}

module "service-pe-gateway-sync-service-variables" {
  source = "[MODULE_URL_PREFIX]/variables/0.1.0-master.zip"

  project     = var.name
  name        = local.pe_gateway_sync_service_name
  environment = var.environment
  tags        = local.tags
  target_path = module.service-pe-gateway-sync-service.variables_target_path

  variables = {
    "ASPNETCORE_ENVIRONMENT"               = var.environment
    "ASPNETCORE_SUPPRESSSTATUSMESSAGES"    = "true"
    "Redis:Url"                            = module.service-pe-gateway-sync-service-elasticache.url
    "SNS:TopicArn"                         = module.service-pe-gateway-sync-service-sns.arn
  }
}

module "service-pe-gateway-sync-service-elasticache" {
  source = "[MODULE_URL_PREFIX]/elasticache/0.1.0-master.zip"

  elasticache_type = "cache.t2.medium"

  project     = var.name
  name        = local.pe_gateway_sync_elasticache_name
  environment = var.environment
  tags        = local.tags
}

module "service-pe-gateway-sync-service-sns" {
  source = "[MODULE_URL_PREFIX]/sns/0.1.0-master.zip"

  project     = var.name
  name        = local.pe_gateway_sync_sns_name
  environment = var.environment
  tags        = local.tags
}

module "service-pe-gateway-sync-service-ecr" {
  source = "[MODULE_URL_PREFIX]/ecr/0.1.0-master.zip"

  project     = var.name
  name        = local.pe_gateway_sync_service_name
  environment = var.environment
  tags        = local.tags
}

output "service-pe-gateway-sync-service" {
  value = {
    name              = local.pe_gateway_sync_service_name

    ecr_url           = module.service-pe-gateway-sync-service-ecr.url
    elasticache       = module.service-pe-gateway-sync-service-elasticache.url
    endpoint          = module.service-pe-gateway-sync-service.endpoint

    sns_topic_arn     = module.service-pe-gateway-sync-service-sns.arn
    
    oag_endpoint      = module.service-pe-gateway-sync-service.oag_endpoint
    oag_endpoint_full = module.service-pe-gateway-sync-service.oag_endpoint_full
  }
}
*/