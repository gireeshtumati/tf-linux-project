/*
locals {
  service_translations_service_name = "translations-service"
}

module "service-translations-service" {
  source = "[MODULE_URL_PREFIX]/service-fargate/0.1.0-master.zip"

  providers = {
    aws         = aws
    aws.route53 = aws.route53
  }

  instances       = local.fargate_count 
  limits_cpu      = local.fargate_cpu
  limits_memory   = local.fargate_memory
  healthcheck_url = "/hc"

  oag_template_create = "true"

  project     = var.name
  name        = local.service_translations_service_name
  environment = var.environment
  tags        = local.tags
}

module "service-translations-service-variables" {
  source = "[MODULE_URL_PREFIX]/variables/0.1.0-master.zip"

  project     = var.name
  name        = local.service_translations_service_name
  environment = var.environment
  tags        = local.tags
  target_path = module.service-translations-service.variables_target_path

  variables = {
    "ASPNETCORE_ENVIRONMENT"             = var.environment
    "ASPNETCORE_SUPPRESSSTATUSMESSAGES"  = "true"
  }
}

module "service-translations-service-ecr" {
  source = "[MODULE_URL_PREFIX]/ecr/0.1.0-master.zip"

  project     = var.name
  name        = local.service_translations_service_name
  environment = var.environment
  tags        = local.tags
}

output "service-translations-service" {
  value = {
    name              = local.service_translations_service_name

    ecr_url           = module.service-translations-service-ecr.url
    endpoint          = module.service-translations-service.endpoint
    endpoint_full     = module.service-translations-service.endpoint_full
    host              = module.service-translations-service.host

    oag_endpoint      = module.service-translations-service.oag_endpoint
    oag_endpoint_full = module.service-translations-service.oag_endpoint_full
  }
}
*/