/*
locals {
  service_abp_calculation_prepost_calc_nodes_prefix          = "abp-cpc-node"
  service_abp_calculation_prepost_calc_variables_target_path = "shared-artifacts/service-variables/${var.name}/${local.service_abp_calculation_prepost_calc_nodes_prefix}-1"
  service_abp_calculation_sqs_prepost_calc_name              = "abp-calculation-sqs-prepost-calc"
}

module "service-abp-cpc-node-1" {
  source = "[MODULE_URL_PREFIX]/windows-ansible/0.1.0-master.zip"

  providers = {
    aws         = aws
    aws.route53 = aws.route53
  }

  project     = var.name
  name        = "${local.service_abp_calculation_prepost_calc_nodes_prefix}-1"
  environment = var.environment
  tags        = var.tags

# t3 instances require AMI supporting Elastic Network Adapter
  ami_windows_2012 = "ami-08493d85988735216"
  instance_type    = "t2.xlarge" 
  
  startup_script = <<EOF
  $install_dir = "c:\install"
  New-Item -ItemType Directory -Force -Path $install_dir
  Push-Location -Path $install_dir
	iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))
  Pop-Location
  # Install .NET Framework
  choco install -y dotnetfx
  choco install -y zip unzip
	EOF
}

module "service-abp-cpc-node-2" {
  source = "[MODULE_URL_PREFIX]/windows-ansible/0.1.0-master.zip"

  providers = {
    aws         = aws
    aws.route53 = aws.route53
  }

  project     = var.name
  name        = "${local.service_abp_calculation_prepost_calc_nodes_prefix}-2"
  environment = var.environment
  tags        = var.tags

# t3 instances require AMI supporting Elastic Network Adapter
  ami_windows_2012 = "ami-08493d85988735216"
  instance_type    = "t2.xlarge"
  
  startup_script = <<EOF
  $install_dir = "c:\install"
  New-Item -ItemType Directory -Force -Path $install_dir
  Push-Location -Path $install_dir
	iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))
  Pop-Location
  # Install .NET Framework
  choco install -y dotnetfx
  choco install -y zip unzip
	EOF
}

module "service-abp-calculation-prepost-calc-variables" {
  source = "[MODULE_URL_PREFIX]/variables/0.1.0-master.zip"

  project     = var.name
  name        = "${local.service_abp_calculation_prepost_calc_nodes_prefix}-1"
  environment = var.environment
  tags        = local.tags

  target_path = local.service_abp_calculation_prepost_calc_variables_target_path

  variables = {
    "DOTNET_ENVIRONMENT"                = var.environment
    "Redis:Url"                         = module.service-abp-calculation-elasticache.url
    "SQS:Url"                           = module.service-abp-calculation-prepost-calc-sqs.url
    "SNS:TopicArn"                      = module.service-abp-calculation-sns.arn
  }
}

resource "aws_iam_policy" "abp-calculation-prepost-calc-policy" {
  name        = "abp-calculation-prepost-calc-policy"
  description = "Full access to SNS and SQS from EC2 instance"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "sns:*"
            ],
            "Effect": "Allow",
            "Resource": "${module.service-abp-calculation-sns.arn}"
        },
        {
            "Action": [
                "sqs:*"
            ],
            "Effect": "Allow",
            "Resource": "${module.service-abp-calculation-prepost-calc-sqs.arn}"
        }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "abp-cpc-attach-node-1" {
  role       = module.service-abp-cpc-node-1.instance_role_name
  policy_arn = aws_iam_policy.abp-calculation-prepost-calc-policy.arn
}

resource "aws_iam_role_policy_attachment" "abp-cpc-attach-node-2" {
  role       = module.service-abp-cpc-node-2.instance_role_name
  policy_arn = aws_iam_policy.abp-calculation-prepost-calc-policy.arn
}

module "service-abp-calculation-prepost-calc-sqs" {
    source = "[MODULE_URL_PREFIX]/sqs/0.1.0-master.zip"

    project = var.name
    name    = local.service_abp_calculation_sqs_prepost_calc_name
    environment = var.environment
    tags        = local.tags

    subscription_sns_count = 1
    subscription_sns_arns  = ["${module.service-abp-calculation-sns.arn}"]
    subscription_sns_filter_policy = <<POLICY
{
  "service": [
    "abp-calculation-prepost-calc"
  ]
}
POLICY
}

output "service-abp-calculation-prepost-calc" {
  value = {
      abp_calculation_prepost_calc_variables_path    = "${module.service-abp-calculation-prepost-calc-variables.s3_bucket}/${module.service-abp-calculation-prepost-calc-variables.s3_key}"
      abp_calculation_prepost_calc_node1_credentials = "${module.service-abp-cpc-node-1.ansible_user}:${module.service-abp-cpc-node-1.ansible_password}"
      abp_calculation_prepost_calc_node2_credentials = "${module.service-abp-cpc-node-2.ansible_user}:${module.service-abp-cpc-node-2.ansible_password}"

      abp_calculation_prepost_calc_sqs_url           = module.service-abp-calculation-prepost-calc-sqs.url
      abp_calculation_prepost_calc_sqs_arn           = module.service-abp-calculation-prepost-calc-sqs.arn
  }
}
*/