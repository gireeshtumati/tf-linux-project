/*
locals {
  service_quantity_calculator_name = "quantity-calculator"
  service_quantity_calculator_redis_storage_name = "qc-redis"
}

module "service-quantity-calculator-webservices" {
  source = "[MODULE_URL_PREFIX]/service-fargate/0.1.0-master.zip"

  providers = {
    aws         = aws
    aws.route53 = aws.route53
  }

  instances       = local.fargate_count 
  limits_cpu      = local.fargate_cpu
  limits_memory   = local.fargate_memory
  healthcheck_url = "/hc"

  oag_template_create = "true"

  project     = var.name
  name        = local.service_quantity_calculator_name
  environment = var.environment
  tags        = local.tags
}

module "service-quantity-calculator-webservices-variables" {
  source = "[MODULE_URL_PREFIX]/variables/0.1.0-master.zip"

  project     = var.name
  name        = local.service_quantity_calculator_name
  environment = var.environment
  tags        = local.tags
  target_path = module.service-quantity-calculator-webservices.variables_target_path

  variables = {
    "ASPNETCORE_ENVIRONMENT"                       = var.environment
    "DocumentService:Url"                          = module.service-document-service-legacy.endpoint
    "DocumentService:IsEnabled"                    = "true"
    "UserSettingsService:Url"                      = module.service-user-settings.endpoint
    "UserSettingsService:IsEnabled"                = "true"
    "Redis:ConnectionString"                       = "${module.service-pe-core-storage-elasticache.url},resolveDns=true,syncTimeout=15000"
    "TranslationsService:Url"                      = module.service-translations-service.endpoint
    "PEWebServices:WebServiceUrl"                  = module.service-pe-core-webservices.endpoint
    "PEWebServices:IsWebServiceEnabled"            = "true"
    "DataAccess:PurchaserDatabaseConnectionString" = module.service-quantity-calculator-database-rds.connection_string
    "DataAccess:TechnicalDatabaseConnectionString" = module.database-technical-database-rds.connection_string
  }
}

module "service-quantity-calculator-webservices-ecr" {
  source = "[MODULE_URL_PREFIX]/ecr/0.1.0-master.zip"

  project     = var.name
  name        = local.service_quantity_calculator_name
  environment = var.environment
  tags        = local.tags
}

module "service-quantity-calculator-storage-elasticache" {
  source = "[MODULE_URL_PREFIX]/elasticache/0.1.0-master.zip"

  elasticache_type = "cache.t2.medium"

  project     = var.name
  name        = local.service_quantity_calculator_redis_storage_name
  environment = var.environment
  tags        = local.tags
}

module "service-quantity-calculator-database-rds" {
  source = "[MODULE_URL_PREFIX]/rds/0.1.0-master.zip"

  providers = {
    aws = aws
  }

  project     = var.name
  name        = local.service_quantity_calculator_name
  environment = var.environment
  tags        = local.tags
    
  rds_output_name = "Hilti_PROFIS3_Purchaser"
  rds_engine = "sqlserver-web"
  rds_engine_version = "14.00.3223.3.v1"
  rds_instance_class = "db.t2.medium"
  rds_port = "1433"
  rds_allocated_storage = "30"
  rds_license = "license-included"
  rds_password_use_special = false
  rds_create_mssql_backup_bucket = true
}

output "service-quantity-calculator" {
  value = {
    quantity_calculator_web_services_name              = local.service_quantity_calculator_name

    quantity_calculator_web_services_ecr_url           = module.service-quantity-calculator-webservices-ecr.url
    quantity_calculator_web_services_endpoint          = module.service-quantity-calculator-webservices.endpoint
    quantity_calculator_web_services_endpoint_full     = module.service-quantity-calculator-webservices.endpoint_full
    quantity_calculator_web_services_host              = module.service-quantity-calculator-webservices.host

    quantity_calculator_web_services_oag_endpoint      = module.service-quantity-calculator-webservices.oag_endpoint
    quantity_calculator_web_services_oag_endpoint_full = module.service-quantity-calculator-webservices.oag_endpoint_full

    quantity_calculator_database_connection_string = module.service-quantity-calculator-database-rds.connection_string
    quantity_calculator_database_user              = module.service-quantity-calculator-database-rds.user
    quantity_calculator_database_password          = module.service-quantity-calculator-database-rds.password
    quantity_calculator_database_host              = module.service-quantity-calculator-database-rds.host
    quantity_calculator_database_name              = module.service-quantity-calculator-database-rds.name

    quantity_calculator_storage_redis_url          = module.service-quantity-calculator-storage-elasticache.url
  }
}
*/