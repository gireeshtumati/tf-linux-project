locals {
  database_technical_database_name = "technical-database"
  #target_path ="${local.infra_prefix}service-variables/${var.project}/${var.module_name}"
  #target_path = ""
}

# module "database-technical-database-variables" {
#   source = "[MODULE_URL_PREFIX]/variables/0.1.0-master.zip"

#   project     = var.name
#   name        = local.database_technical_database_name
#   environment = var.environment
#   tags        = local.tags
#   target_path = "${var.infra_prefix}service-variables/${var.name}/${local.database_technical_database_name}"

#   variables = {
#     "ConnectionString" = "${module.database-technical-database-rds.connection_string}"
#   }
# }

module "database-technical-database-rds" {
  source = "https://s3-eu-west-1.amazonaws.com/hilti-infra-public/shared-artifacts/terraform-modules/rds/0.1.0-china-stage.zip"

  project     = var.name
  name        = local.database_technical_database_name
  environment = var.environment
  tags        = local.tags
    
  rds_output_name = "Nolasoft_PROFIS3_TechnicalDatabase"
  rds_engine = "sqlserver-ex"
  rds_engine_version = "14.00.3223.3.v1"
  rds_instance_class = "db.t2.medium"
  rds_port = "1433"
  rds_allocated_storage = "30"
  rds_license = "license-included"
  rds_password_use_special = false
  rds_create_mssql_backup_bucket = false
}

output "database_technical_database" {
  value = {
    connection_string = "${module.database-technical-database-rds.connection_string}"
    user = "${module.database-technical-database-rds.user}"
    password = "${module.database-technical-database-rds.password}"
    host = "${module.database-technical-database-rds.host}"
    name = "${module.database-technical-database-rds.name}"
  }
}
