/*
locals {
  service_c2c_calculation_service_name = "c2c-calculation-service"
}

module "service-c2c-calculation-service" {
  source = "[MODULE_URL_PREFIX]/service-fargate/0.1.0-master.zip"

  providers = {
    aws         = aws
    aws.route53 = aws.route53
  }

  instances       = local.fargate_count 
  limits_cpu      = local.fargate_cpu
  limits_memory   = local.fargate_memory
  healthcheck_url = "/hc"

  oag_template_create = "true"

  project     = var.name
  name        = local.service_c2c_calculation_service_name
  environment = var.environment
  tags        = local.tags
}

module "service-c2c-calculation-service-variables" {
  source = "[MODULE_URL_PREFIX]/variables/0.1.0-master.zip"

  project     = var.name
  name        = local.service_c2c_calculation_service_name
  environment = var.environment
  tags        = local.tags
  target_path = module.service-c2c-calculation-service.variables_target_path

  variables = {
    "ASPNETCORE_ENVIRONMENT"              = var.environment
  }
}

module "service-c2c-calculation-service-ecr" {
  source = "[MODULE_URL_PREFIX]/ecr/0.1.0-master.zip"

  project     = var.name
  name        = local.service_c2c_calculation_service_name
  environment = var.environment
  tags        = local.tags
}

output "service-c2c-calculation-service" {
  value = {
    name              = local.service_c2c_calculation_service_name

    ecr_url           = module.service-c2c-calculation-service-ecr.url
    endpoint          = module.service-c2c-calculation-service.endpoint
    endpoint_full     = module.service-c2c-calculation-service.endpoint_full
    host              = module.service-c2c-calculation-service.host

    oag_endpoint      = module.service-c2c-calculation-service.oag_endpoint
    oag_endpoint_full = module.service-c2c-calculation-service.oag_endpoint_full
  }
}
*/