/*
locals {
  service_webapi_name = "webapi"
  webapi_jwttoken_usersa = false
  webapi_audiences = "urn:hc:api"
  webapi_jwttoken_signingpublickeyxmlfile = "rsa-public-key.xml"
  webapi_authority = "https://hilti-idm-sit.eu.auth0.com/"
  webapi_jwttoken_signingpriavtekey = "=g;kZ@KFR@*W=G=Mmt[pdwULq>A>J{qG-`}Y9t^`Cs4Tm5^c_M6L}v@K3mfwT8g'"
  webapi_gatewayaccesstokenheader = "GatewayAuthorization"
  webapi_claimprefix = "$PEAPI$"
  webapi_sqldatabase_includenottesteddata = true
  webapi_mongodatabase_connectionstring = "mongodb+srv://c2cdev:pa$$w0rd@c2cdev-zwrrw.mongodb.net/test?retryWrites=true&w=majority"
  webapi_mongodatabase_name = "Hilti_C2C_CodeListService" 
  webapi_publishdelayinsecond = 3
  webapi_prometheus_httprequestcountermetrics_name = "externalwebapi_requests_total"
  webapi_prometheus_httprequestcountermetrics_help = "externalwebapi:Provides the count of requests received"
  webapi_prometheus_httprequestdurationmetrics_name = "externalwebapi_requests_duration_seconds"
  webapi_prometheus_httprequestdurationmetrics_help = "externalwebapi:Provides the duration of requests executed"
  webapi_prometheus_httprequestinprogressmetrics_name = "externalwebapi_requests_inprogress"
  webapi_prometheus_httprequestinprogressmetrics_help = "externalwebapi:Provides the number of requests currently in progress"
  webapi_prometheus_healthchecksqlservergaugemetrics_name = "externalwebapi_sqlserver_unhealthy"
  webapi_prometheus_healthchecksqlservergaugemetrics_help = "externalwebapi:Provides the Gauge of unhealthy condition for SQL Server"
  webapi_prometheus_healthcheckmongodbgaugemetrics_name = "externalwebapi_mongodb_unhealthy"
  webapi_prometheus_healthcheckmongodbgaugemetrics_help = "externalwebapi:Provides the Gauge of unhealthy condition for Mongo DB"
  webapi_prometheus_healthcheckpostgresdbgaugemetrics_name = "externalwebapi_postgresdb_unhealthy"
  webapi_prometheus_healthcheckpostgresdbgaugemetrics_help = "externalwebapi:Provides the Gauge of unhealthy condition for Postgres DB"
  webapi_prometheus_healthcheckredisgaugemetrics_name = "externalwebapi_redis_unhealthy"
  webapi_prometheus_healthcheckredisgaugemetrics_help = "externalwebapi:Provides the Gauge of unhealthy condition for Redis"
  webapi_prometheus_healthcheckothergaugemetrics_name = "externalwebapi_other_unhealthy"
  webapi_prometheus_healthcheckothergaugemetrics_help = "externalwebapi:Provides the Gauge of unhealthy condition for Other"
}

module "service-webapi" {
  source = "[MODULE_URL_PREFIX]/service-fargate/0.1.0-master.zip"

  providers = {
    aws         = aws
    aws.route53 = aws.route53
  }

  instances       = local.fargate_count 
  limits_cpu      = local.fargate_cpu
  limits_memory   = local.fargate_memory
  healthcheck_url = "/hc"

  oag_template_create = "true"

  project     = var.name
  name        = local.service_webapi_name
  environment = var.environment
  tags        = local.tags
}

module "service-webapi-variables" {
  source = "[MODULE_URL_PREFIX]/variables/0.1.0-master.zip"

  project     = var.name
  name        = local.service_webapi_name
  environment = var.environment
  tags        = local.tags
  target_path = module.service-webapi.variables_target_path

  variables = {
    "ASPNETCORE_ENVIRONMENT"              = var.environment
	"Authentication:Authority"            = local.webapi_authority
	"Authentication:Audiences"            = local.webapi_audiences
	"Authentication:JWTTokenUseRsa"       = local.webapi_jwttoken_usersa
	"Authentication:JWTTokenPrivateKey"   = local.webapi_jwttoken_signingpriavtekey
	"Authentication:JWTTokenPublicKeyXmlFile"  = local.webapi_jwttoken_signingpublickeyxmlfile
	"Authentication:ClaimPrefix"          = local.webapi_claimprefix
        "Authentication:GatewayAccessTokenHeader" = local.webapi_gatewayaccesstokenheader
      "SQLDatabase:ConnectionString"           = "${module.database-technical-database-rds.connection_string}"
      "SQLDatabase:IncludeNotTestedData"       = local.webapi_sqldatabase_includenottesteddata
      "MongoDatabase:ConnectionString"           = local.webapi_mongodatabase_connectionstring
      "MongoDatabase:Name"       = local.webapi_mongodatabase_name
      "HealthCheck:PublishDelayInSecond" = local.webapi_publishdelayinsecond
      "Prometheus:HttpRequestCounterMetrics:Name" = local.webapi_prometheus_httprequestcountermetrics_name
      "Prometheus:HttpRequestCounterMetrics:Help" = local.webapi_prometheus_httprequestcountermetrics_help
      "Prometheus:HttpRequestDurationMetrics:Name" = local.webapi_prometheus_httprequestdurationmetrics_name
      "Prometheus:HttpRequestDurationMetrics:Help" = local.webapi_prometheus_httprequestdurationmetrics_help
      "Prometheus:HttpRequestInProgressMetrics:Name" = local.webapi_prometheus_httprequestinprogressmetrics_name
      "Prometheus:HttpRequestInProgressMetrics:Help" = local.webapi_prometheus_httprequestinprogressmetrics_help
      "Prometheus:HealthCheckSQLServerGaugeMetrics:Name" = local.webapi_prometheus_healthchecksqlservergaugemetrics_name
      "Prometheus:HealthCheckSQLServerGaugeMetrics:Help" = local.webapi_prometheus_healthchecksqlservergaugemetrics_help
      "Prometheus:HealthCheckMongoDBGaugeMetrics:Name" = local.webapi_prometheus_healthcheckmongodbgaugemetrics_name
      "Prometheus:HealthCheckMongoDBGaugeMetrics:Help" = local.webapi_prometheus_healthcheckmongodbgaugemetrics_help
      "Prometheus:HealthCheckPostgresDBGaugeMetrics:Name" = local.webapi_prometheus_healthcheckpostgresdbgaugemetrics_name
      "Prometheus:HealthCheckPostgresDBGaugeMetrics:Help" = local.webapi_prometheus_healthcheckpostgresdbgaugemetrics_help
      "Prometheus:HealthCheckRedisGaugeMetrics:Name" = local.webapi_prometheus_healthcheckredisgaugemetrics_name
      "Prometheus:HealthCheckRedisGaugeMetrics:Help" = local.webapi_prometheus_healthcheckredisgaugemetrics_help
      "Prometheus:HealthCheckOtherGaugeMetrics:Name" = local.webapi_prometheus_healthcheckothergaugemetrics_name
      "Prometheus:HealthCheckOtherGaugeMetrics:Help" = local.webapi_prometheus_healthcheckothergaugemetrics_help
    }
}


module "service-webapi-ecr" {
  source = "[MODULE_URL_PREFIX]/ecr/0.1.0-master.zip"

  project     = var.name
  name        = local.service_webapi_name
  environment = var.environment
  tags        = local.tags
}

output "service-webapi" {
  value = {
    name              = local.service_webapi_name

    ecr_url           = module.service-webapi-ecr.url
    endpoint          = module.service-webapi.endpoint
    endpoint_full     = module.service-webapi.endpoint_full
    host              = module.service-webapi.host

    oag_endpoint      = module.service-webapi.oag_endpoint
    oag_endpoint_full = module.service-webapi.oag_endpoint_full
  }
}
*/