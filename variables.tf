variable "module_version" {
  type        = string
  description = "Version of the current module"
}

variable "module_name" {
  type        = string
  description = "Name of the current module"
  default     = "project-pe"
}

variable "project" {
  type = string
}

variable "environment" {
  type = string
}

variable "aws_region" {
  type = string
}

variable "infra_bucket" {
  type = string
}

variable "release" {
  type = string
}

